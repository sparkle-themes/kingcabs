<?php
/**
 * Includes all the custom controller classes
 *
 * @param string $file_path , path from the theme
 * @return string full path of file inside theme
 *
 */

require get_template_directory() .'/inc/custom-controller/tab/class-tab-controller.php';
require get_template_directory() .'/inc/custom-controller/switch/class-switch.php';
require get_template_directory() .'/inc/custom-controller/background-control/background-control.php';
require get_template_directory() .'/inc/custom-controller/cssbox/class-control-cssbox.php';