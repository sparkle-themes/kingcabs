<div class="free-vs-pro-info-wrap">
    <div class="free-vs-pro-info">
        <h4><?php esc_html_e('ONE TIME PAYMENT', 'kingcabs'); ?></h4>
        <p><?php esc_html_e('No renewal needed', 'kingcabs'); ?></p>
    </div>

    <div class="free-vs-pro-info">
        <h4><?php esc_html_e('UNLIMITED DOMAIN LICENCE', 'kingcabs'); ?></h4>
        <p><?php esc_html_e('Use in as many websites as you need', 'kingcabs'); ?></p>
    </div>

    <div class="free-vs-pro-info">
        <h4><?php esc_html_e('FREE UPDATES FOR LIFETIME', 'kingcabs'); ?></h4>
        <p><?php esc_html_e('Keep up to date', 'kingcabs'); ?></p>
    </div>
</div>

<table class="comparison-table">
	<thead>
		<tr>
			<th>
				<a href="<?php echo esc_url('https://sparklewpthemes.com/wordpress-themes/limousine-wordpress-theme/'); ?>" target="_blank" class="button button-primary button-hero">
					<?php esc_html_e( 'Get Kingcabs Pro', 'kingcabs' ); ?>
				</a>
			</th>
			<th class="compare-icon"><?php esc_html_e( 'Kingcabs', 'kingcabs' ); ?></th>
			<th class="compare-icon"><?php esc_html_e( 'Kingcabs Pro', 'kingcabs' ); ?></th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>
				<h3><?php esc_html_e( 'Maintaince Mode Included', 'kingcabs' ); ?></h3>
			</td>
			<td class="compare-icon"><span class="dashicons-before dashicons-no"></span></td>
			<td class="compare-icon"><span class="dashicons-before dashicons-yes"></span></td>
		</tr>

		<tr>
			<td>
				<h3><?php esc_html_e( 'GDPR Compliance & Cookies Consent', 'kingcabs' ); ?></h3>
			</td>
			<td class="compare-icon"><span class="dashicons-before dashicons-no"></span></td>
			<td class="compare-icon"><span class="dashicons-before dashicons-yes"></span></td>
		</tr>

		<tr>
			<td>
				<h3><?php esc_html_e( 'Unlimited Color Options', 'kingcabs' ); ?></h3>
			</td>
			<td class="compare-icon"><span class="dashicons-before dashicons-no"></span></td>
			<td class="compare-icon"><span class="dashicons-before dashicons-yes"></span></td>
		</tr>
		<tr>
			<td>
				<h3><?php esc_html_e( 'Slider Type and Layout Options', 'kingcabs' ); ?></h3>
			</td>
			<td class="compare-icon"><span class="dashicons-before dashicons-no"></span></td>
			<td class="compare-icon"><span class="dashicons-before dashicons-yes"></span></td>
		</tr>
		<tr>
			<td>
				<h3><?php esc_html_e( 'Multiple Header Layout and Settings', 'kingcabs' ); ?></h3>
			</td>
			<td class="compare-icon"><span class="dashicons-before dashicons-no"></span></td>
			<td class="compare-icon"><span class="dashicons-before dashicons-yes"></span></td>
		</tr>
		<tr>
			<td>
				<h3><?php esc_html_e( 'Advanced Footer Setting', 'kingcabs' ); ?></h3>
			</td>
			<td class="compare-icon"><span class="dashicons-before dashicons-no"></span></td>
			<td class="compare-icon"><span class="dashicons-before dashicons-yes"></span></td>
		</tr>
		<tr>
			<td>
				<h3><?php esc_html_e( 'Advanced Blog Layout Settings', 'kingcabs' ); ?></h3>
			</td>
			<td class="compare-icon"><span class="dashicons-before dashicons-no"></span></td>
			<td class="compare-icon"><span class="dashicons-before dashicons-yes"></span></td>
		</tr>
		<tr>
			<td>
				<h3><?php esc_html_e( '1-Click Automatic Updates', 'kingcabs' ); ?></h3>
			</td>
			<td class="compare-icon"><span class="dashicons-before dashicons-yes"></span></td>
			<td class="compare-icon"><span class="dashicons-before dashicons-yes"></span></td>
		</tr>
		<tr>
			<td>
				<h3><?php esc_html_e( 'Footer Copyright Editor', 'kingcabs' ); ?></h3>
			</td>
			<td class="compare-icon"><span class="dashicons-before dashicons-no"></span></td>
			<td class="compare-icon"><span class="dashicons-before dashicons-yes"></span></td>
		</tr>
		<tr>
			<td>
				<h3><?php esc_html_e( 'Reorder All Sections', 'kingcabs' ); ?></h3>
			</td>
			<td class="compare-icon"><span class="dashicons-before dashicons-no"></span></td>
			<td class="compare-icon"><span class="dashicons-before dashicons-yes"></span></td>
		</tr>
		<tr>
			<td>
				<h3><?php esc_html_e( 'Limousine Fleet Layout', 'kingcabs' ); ?></h3>
			</td>
			<td class="compare-icon"><span class="dashicons-before dashicons-no"></span></td>
			<td class="compare-icon"><span class="dashicons-before dashicons-yes"></span></td>
		</tr>
		
		<tr>
			<td>
				<h3><?php esc_html_e( 'Breadcrumb Layout and Option', 'kingcabs' ); ?></h3>
			</td>
			<td class="compare-icon">Basic</td>
			<td class="compare-icon">Advanced</td>
		</tr>
		<tr>
			<td>
				<h3><?php esc_html_e( 'Preloader Option', 'kingcabs' ); ?></h3>
			</td>
			<td class="compare-icon">Basic</td>
			<td class="compare-icon">Advanced</td>
		</tr>		
		<tr>
			<td>
				<h3><?php esc_html_e( 'SEO Optimized', 'kingcabs' ); ?></h3>
			</td>
			<td class="compare-icon"><span class="dashicons-before dashicons-yes"></span></td>
			<td class="compare-icon"><span class="dashicons-before dashicons-yes"></span></td>
		</tr>
		<tr>
			<td>
				<h3><?php esc_html_e( 'Premium Support 24/7', 'kingcabs' ); ?></h3>
			</td>
			<td class="compare-icon"><span class="dashicons-before dashicons-yes"></span></td>
			<td class="compare-icon"><span class="dashicons-before dashicons-yes"></span></td>
		</tr>
		<tr>
			<td colspan="3">
				<a href="<?php echo esc_url('https://sparklewpthemes.com/wordpress-themes/limousine-wordpress-theme/'); ?>" target="_blank" class="button button-primary button-hero">
					<strong><?php esc_html_e( 'View Full Feature List', 'kingcabs' ); ?></strong>
				</a>
			</td>
		</tr>
	</tbody>
</table>
