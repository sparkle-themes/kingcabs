<div class="welcome-upgrade-wrap">
    <div class="welcome-upgrade-header">
        <h3><?php printf(esc_html__('Premium Version of %s', 'kingcabs'), $this->theme_name); ?></h3>
        <p><?php echo sprintf(esc_html__('Check out the demos that you can create with the premium version of the %s theme. 5+ Pre-defined demos can be imported with just one click in the premium version.', 'kingcabs'), $this->theme_name); ?></p>
    </div>

    <div class="recomended-plugin-wrap">
        <div class="recommended-plugins">
            <div class="plugin-image">
                <img src="https://demo.sparklewpthemes.com/kingcabspro/demos/wp-content/uploads/sites/4/2020/12/demo-01.png">
            </div>

            <div class="plugin-title-wrap">
                <span class="title"><?php esc_html_e('kingcabs Pro - Main','kingcabs'); ?></span>
                <span class="plugin-btn-wrapper">
					<a target="_blank" href="https://demo.sparklewpthemes.com/kingcabspro/" class="button button-primary"><?php echo esc_html__('Preview', 'kingcabs'); ?></a>
				</span>
            </div>
        </div>

        <div class="recommended-plugins">
            <div class="plugin-image">
                <img src="https://demo.sparklewpthemes.com/kingcabspro/demos/wp-content/uploads/sites/4/2020/12/demo-02.png">
            </div>

            <div class="plugin-title-wrap">
                <span class="title"><?php esc_html_e('kingcabs Pro - Two','kingcabs'); ?></span>
				<span class="plugin-btn-wrapper">
					<a target="_blank" href="https://demo.sparklewpthemes.com/kingcabspro/demo1/" class="button button-primary"><?php echo esc_html__('Preview', 'kingcabs'); ?></a>
				</span>
            </div>
        </div>
        
        <div class="recommended-plugins">
            <div class="plugin-image">
                <img src="https://demo.sparklewpthemes.com/kingcabspro/demos/wp-content/uploads/sites/4/2020/12/demo-03.png">
            </div>

            <div class="plugin-title-wrap">
                <span class="title"><?php esc_html_e('kingcabs Pro - Demo 2','kingcabs'); ?></span>
				<span class="plugin-btn-wrapper">
					<a target="_blank" href="https://demo.sparklewpthemes.com/kingcabspro/demo2/" class="button button-primary"><?php echo esc_html__('Preview', 'kingcabs'); ?></a>
				</span>
            </div>
        </div>
        <div class="recommended-plugins">
            <div class="plugin-image">
                <img src="https://demo.sparklewpthemes.com/kingcabspro/demos/wp-content/uploads/sites/4/2020/12/free-demo.png">
            </div>

            <div class="plugin-title-wrap">
                <span class="title"><?php esc_html_e('kingcabs Pro - Three','kingcabs'); ?></span>
				<span class="plugin-btn-wrapper">
					<a target="_blank" href="https://demo.sparklewpthemes.com/kingcabs/" class="button button-primary"><?php echo esc_html__('Preview', 'kingcabs'); ?></a>
				</span>
            </div>
        </div>
        
    </div>
</div>

<div class="welcome-upgrade-box">
    <div class="upgrade-box-text">
        <h3><?php echo esc_html__('Upgrade To Premium Version (14 Days Money Back Guarantee)', 'kingcabs'); ?></h3>
        <p><?php echo sprintf(esc_html__('With %s Pro theme you can create a beautiful website. If you want to unlock more possibilities then upgrade to the premium version, try the Premium version and check if it fits your need or not. If not, we have 14 days money-back guarantee.', 'kingcabs'), $this->theme_name); ?></p>
    </div>

    <a class="upgrade-button" href="https://sparklewpthemes.com/wordpress-themes/limousine-wordpress-theme/" target="_blank"><?php esc_html_e('Upgrade Now', 'kingcabs'); ?></a>
</div>

