<?php
/**
 * King_Cabs: Block Patterns
 *
 * @since King_Cabs 1.1.5
 */

/**
 * Registers block patterns and categories.
 *
 * @since King_Cabs 1.1.5
 *
 * @return void
 */
function kingcabs_register_block_patterns() {

	$patterns = array();

	$block_pattern_categories = array(
		'kingcabs' => array( 'label' => __( 'King Cabs', 'kingcabs' ) )
	);

	/**
	 * Filters the theme block pattern categories.
	 *
	 * @since King_Cabs 1.1.5
	 *
	 * @param array[] $block_pattern_categories {
	 *     An associative array of block pattern categories, keyed by category name.
	 *
	 *     @type array[] $properties {
	 *         An array of block category properties.
	 *
	 *         @type string $label A human-readable label for the pattern category.
	 *     }
	 * }
	 */
	$block_pattern_categories = apply_filters( 'kingcabs_block_pattern_categories', $block_pattern_categories );

	foreach ( $block_pattern_categories as $name => $properties ) {
		if ( ! WP_Block_Pattern_Categories_Registry::get_instance()->is_registered( $name ) ) {
			register_block_pattern_category( $name, $properties );
		}
	}
}
add_action( 'init', 'kingcabs_register_block_patterns', 9 );
