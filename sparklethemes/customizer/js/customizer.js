/**
 * File customizer.js.
 *
 * Theme Customizer enhancements for a better user experience.
 *
 * Contains handlers to make Theme Customizer preview reload changes asynchronously.
 */

( function( $ ) {

	// Site title and description.
	wp.customize( 'blogname', function( value ) {
		value.bind( function( to ) {
			$( '.site-title a' ).text( to );
		} );
	} );
	wp.customize( 'blogdescription', function( value ) {
		value.bind( function( to ) {
			$( '.site-description' ).text( to );
		} );
	} );

	// Header text color.
	wp.customize( 'header_textcolor', function( value ) {
		value.bind( function( to ) {
			if ( 'blank' === to ) {
				$( '.site-title, .site-description' ).css( {
					'clip': 'rect(1px, 1px, 1px, 1px)',
					'position': 'absolute'
				} );
			} else {
				$( '.site-title, .site-description' ).css( {
					'clip': 'auto',
					'position': 'relative'
				} );
				$( '.site-title a, .site-description' ).css( {
					'color': to
				} );
			}
		} );
	} );

	wp.customize( 'kingcabs_header_layout', function( value ) {
		value.bind( function( val ) {
			$( '.main-header' ).removeClass( 'headerone headertwo' ).addClass( val );
		});
	});

	wp.customize( 'kingcabs_header_phone', function( value ) {
		value.bind( function( val ) {
			$( '.logo-right-info.k-phone ul li a' ).html( val );
		});
	});

	wp.customize( 'kingcabs_header_button_title', function( value ) {
		value.bind( function( val ) {
			$( '.logo-right-info.k-button a' ).html( '<i class="fa fa-chevron-right"></i>' + val );
		});
	});

	wp.customize( 'kingcabs_header_button_url', function( value ) {
		value.bind( function( val ) {
			$( '.logo-right-info.k-button a' ).attr( 'href', val );
		});
	});

	wp.customize( 'kingcabs_image_carousel_button_title', function( value ) {
		value.bind( function( val ) {
			$( '.banner-slider .item .caption-left a' ).html( val );
		});
	});

	wp.customize( 'kingcabs_service_title', function( value ) {
		value.bind( function( val ) {
			$( '.kingcabs-service-section .section-title h2' ).text( val );
		});
	});

	wp.customize( 'kingcabs_service_sub_title', function( value ) {
		value.bind( function( val ) {
			$( '.kingcabs-service-section .section-title .desc-text' ).html( val );
		});
	});

	wp.customize( 'kingcabs_service_page_title_icon', function( value ) {
		value.bind( function( val ) {
			$( '.kingcabs-service-section .section-title .after i' ).removeClass().addClass( val );
		});
	});

	wp.customize( 'kingcabs_service_left_bg', function( value ) {
		value.bind( function( val ) {
			$( '.kingcabs-service-section .feature-image img' ).attr( 'src', val );
		});
	});

	wp.customize( 'kingcabs_services_title', function( value ) {
		value.bind( function( val ) {
			$( '.kingcabs-main-services .section-title h2' ).text( val );
		});
	});

	wp.customize( 'kingcabs_services_sub_title', function( value ) {
		value.bind( function( val ) {
			$( '.kingcabs-main-services .section-title .desc-text' ).html( val );
		});
	});
	
	wp.customize( 'kingcabs_services_icon_title', function( value ) {
		value.bind( function( val ) {
			$( '.kingcabs-main-services .section-title .after i' ).removeClass().addClass( val );
		});
	});

	wp.customize( 'kingcabs_counter_title', function( value ) {
		value.bind( function( val ) {
			$( '.kingcabs-counter .section-title h2' ).html( val );
		});
	});

	wp.customize( 'kingcabs_counter_sub_title', function( value ) {
		value.bind( function( val ) {
			$( '.kingcabs-counter .section-title .desc-text' ).html( val );
		});
	});

	wp.customize( 'kingcabs_counter_bg', function( value ) {
		value.bind( function( val ) {
			$( '.kingcabs-counter' ).css( 'background-image', 'url(' + val + ')' );
		});
	});

	wp.customize( 'kingcabs_counter_icon_title', function( value ) {
		value.bind( function( val ) {
			$( '.kingcabs-counter .section-title .after i' ).removeClass().addClass( val );
		});
	});

	wp.customize( 'kingcabs_counter_style', function( value ) {
		value.bind( function( val ) {
			$( '.kingcabs-counter' ).removeClass( 'style1 style2' ).addClass( val );
		});	
	});

	wp.customize( 'kingcabs_fleet_title', function( value ) {
		value.bind( function( val ) {
			$( '.kingcabs-fleet .section-title h2' ).html( val );
		});
	});

	wp.customize( 'kingcabs_fleet_sub_title', function( value ) {
		value.bind( function( val ) {
			$( '.kingcabs-fleet .section-title .desc-text' ).html( val );
		});
	});

	wp.customize( 'kingcabs_fleet_icon_title', function( value ) {
		value.bind( function( val ) {
			$( '.kingcabs-fleet .section-title .after i' ).removeClass().addClass( val );
		});
	});

	wp.customize( 'kingcabs_fleet_button_title', function( value ) {
		value.bind( function( val ) {
			$( '.kingcabs-fleet .gallery-content a.btn-primary' ).html( val );
		});
	});

	wp.customize( 'kingcabs_fleet_button_url', function( value ) {
		value.bind( function( val ) {
			$( '.kingcabs-fleet .gallery-content a.btn-primary' ).attr( 'href', val );
		});
	});

	wp.customize( 'kingcabs_testimonial_title', function( value ) {
		value.bind( function( val ) {
			$( '.kingcabs-testimonials .section-title h2' ).html( val );
		});
	});

	wp.customize( 'kingcabs_testimonial_sub_title', function( value ) {
		value.bind( function( val ) {
			$( '.kingcabs-testimonials .section-title .desc-text' ).html( val );
		});
	});

	wp.customize( 'kingcabs_testimonial_icon_title', function( value ) {
		value.bind( function( val ) {
			$( '.kingcabs-testimonials .section-title .after i' ).removeClass().addClass( val );
		});
	});	

	wp.customize( 'kingcabs_testimonial_section_layout', function( value ) {
		value.bind( function( val ) {
			$( '.kingcabs-testimonials' ).removeClass( 'layoutone layouttwo' ).addClass( val );
		});
	});

	jQuery(document).ready( function() {
        wp.customize.selectiveRefresh.bind( 'partial-content-rendered', function( placement ) {
            if ( placement.partial.id === 'kingcabs_testimonial' ) {
				jQuery(".kc-testimonials").lightSlider({
					item: 3,
					pager: true,
					loop: true,
					speed: 600,
					controls: false,
					slideMargin: 20,
					auto: false,
					pauseOnHover: true,
					onSliderLoad: function () {
						jQuery('.kc-testimonials').removeClass('cS-hidden');
					},
					responsive: [
						{
							breakpoint: 800,
							settings: {
								item: 2,
								slideMove: 1,
								slideMargin: 6,
							}
						},
						{
							breakpoint: 480,
							settings: {
								item: 1,
								slideMove: 1,
							}
						}
					]
				});
            }
        });
    });

	wp.customize( 'kingcabs_driver_title', function( value ) {
		value.bind( function( val ) {
			$( '.kingcabs-team .section-title h2' ).html( val );
		});
	});

	wp.customize( 'kingcabs_driver_sub_title', function( value ) {
		value.bind( function( val ) {
			$( '.kingcabs-team .section-title .desc-text' ).html( val );
		});
	});

	wp.customize( 'kingcabs_driver_page_title_icon', function( value ) {
		value.bind( function( val ) {
			$( '.kingcabs-team .section-title .after i' ).removeClass().addClass( val );
		});
	});	

	jQuery(document).ready( function() {
        wp.customize.selectiveRefresh.bind( 'partial-content-rendered', function( placement ) {
            if ( placement.partial.id === 'kingcabs_driver' ) {
				var teamcolumn = wp.customize( 'kingcabs_driver_column' ).get();
				jQuery(".team-carousel").lightSlider({
					item: teamcolumn,
					pager: false,
					loop: true,
					speed: 600,
					controls: true,
					slideMargin: 20,
					auto: false,
					pauseOnHover: true,
					onSliderLoad: function () {
						jQuery('.team-carousel').removeClass('cS-hidden');
					},
					responsive: [
						{
							breakpoint: 800,
							settings: {
								item: 2,
								slideMove: 1,
								slideMargin: 6,
							}
						},
						{
							breakpoint: 480,
							settings: {
								item: 1,
								slideMove: 1,
							}
						}
					]
				});
            }
        });
    });

	wp.customize( 'kingcabs_call_to_action_button_text', function( value ) {
		value.bind( function( val ) {
			$( '.kingcabs-call-us .call-us-text h3' ).html( val );
		});
	});

	wp.customize( 'kingcabs_call_to_action_button_number', function( value ) {
		value.bind( function( val ) {
			$( '.kingcabs-call-us .call-us-text h2 a' ).text( val );
			$( '.kingcabs-call-us .call-us-text h2 a' ).attr( 'href', 'tel:' + val );
		});
	});

	jQuery(document).ready( function() {
        wp.customize.selectiveRefresh.bind( 'partial-content-rendered', function( placement ) {
            if ( placement.partial.id === 'kingcabs_client_logo' ) {
				jQuery(".happy-clients").lightSlider({
					item: 4,
					pager: false,
					loop: true,
					speed: 600,
					controls: false,
					slideMargin: 20,
					auto: true,
					pauseOnHover: true,
					onSliderLoad: function () {
						jQuery('.team-carousel').removeClass('cS-hidden');
					},
					responsive: [
						{
							breakpoint: 800,
							settings: {
								item: 2,
								slideMove: 1,
								slideMargin: 6,
							}
						},
						{
							breakpoint: 480,
							settings: {
								item: 1,
								slideMove: 1,
							}
						}
					]
				});
            }
        });
    });

	wp.customize( 'kingcabs_footer_top_title', function( value ) {
		value.bind( function( val ) {
			$( '.footerup .call-to-action h2 span' ).html( '<strong>' + val + '</strong>' );
		});
	});

	wp.customize( 'kingcabs_footer_top_button_title', function( value ) {
		value.bind( function( val ) {
			$( '.footerup .call-to-action a' ).html( '<i class="fa fa-chevron-right">' + val + '</i>' );
		});
	});

	wp.customize( 'kingcabs_footer_top_button_title', function( value ) {
		value.bind( function( val ) {
			$( '.footerup .call-to-action a' ).html( '<i class="fa fa-chevron-right">' + val + '</i>' );
		});
	});

	wp.customize( 'kingcabs_footer_top_button_url_title', function( value ) {
		value.bind( function( val ) {
			$( '.footerup .call-to-action a' ).attr( 'href', val );
		});
	});

} )( jQuery );
