<?php
/**
 * King Cabs Theme Customizer
 *
 * @package King_Cabs
 */
/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function kingcabs_customize_register( $wp_customize ) {


	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	/**
	 * List All Pages
	*/
    $categories = get_categories();
    $cat_lists = array();
    foreach( $categories as $category ) {
        $cat_lists[$category->term_id] = $category->name;
    }
    
	$kingcabs_pages = get_pages(array('hide_empty' => 0));
	foreach ($kingcabs_pages as $kingcabs_pages_single) {
		$kingcabs_page_choice[$kingcabs_pages_single->ID] = $kingcabs_pages_single->post_title; 
	}

	/**
	 * Option to get the frontpage settings to the old default template if a static frontpage is selected
	*/

	$wp_customize->get_section('static_front_page' )->priority = 2;
	$wp_customize->register_section_type('KingCabs_Customize_Upgrade_Section');

	$wp_customize->add_setting( 'kingcabs_set_original_fp', array(
		'sanitize_callback' => 'sanitize_text_field',
		'default' => false
	));

	$wp_customize->add_control( 'kingcabs_set_original_fp', array(
		'type' => 'checkbox',
		'label' => __( 'Kingcabs Front Page','kingcabs' ),
		'section' => 'static_front_page',
		'priority' => 9
	));


/**
 * Themes Color Settings
*/	
$wp_customize->get_section('colors' )->title = __('Colors Settings', 'kingcabs');

$wp_customize->add_setting('colors_upgrade_text', array(
    'sanitize_callback' => 'kingcabs_sanitize_text'
));

$wp_customize->add_control(new KingCabs_Upgrade_Text($wp_customize, 'colors_upgrade_text', array(
    'section' => 'colors',
    'label' => __('For more settings,', 'kingcabs'),
    'choices' => array(
        __('Supports Primary Theme Colors', 'kingcabs'),
    ),
    'priority' => 100
)));

$wp_customize->add_setting('header_image_upgrade_text', array(
    'sanitize_callback' => 'kingcabs_sanitize_text'
));

$wp_customize->add_control(new KingCabs_Upgrade_Text($wp_customize, 'header_image_upgrade_text', array(
    'section' => 'header_image',
    'label' => __('For more settings,', 'kingcabs'),
    'choices' => array(
        __('Supports Your Own Header Video', 'kingcabs'),
        __('Header Video can also be embedded from Youtube', 'kingcabs'),
    ),
    'priority' => 100
)));

/*============HOME PANEL============*/
	$wp_customize->add_panel(
		'kingcabs_home_panel',
		array(
			'title' => __( 'Theme Options', 'kingcabs' ),
			'priority' => 86
		)
	);

/*============HEADER SETTING SECTION============*/
	$wp_customize->add_section(
		'kingcabs_header_section',
		array(
			'title' => __( 'Header', 'kingcabs' ),
			'panel' => 'kingcabs_home_panel',
			'priority' => 1,
		)
	);

	add_filter("kingcabs_header_nav_content_tab", function() {

		return [
			'kingcabs_header_layout',
			'kingcabs_header_phone',
			'kingcabs_header_button_title',
			'kingcabs_header_button_url',
			'kingcabs_header_link_to_social',
		];

	});

	$wp_customize->add_setting('kingcabs_header_layout', array(
		'default' => 'headerone',
		'sanitize_callback' => 'sanitize_text_field',  //done
		'transport' => 'postMessage',
    ));

    $wp_customize->add_control('kingcabs_header_layout', array(
		'type' => 'select',
		'label' => __('Layout', 'kingcabs'),
		'section' => 'kingcabs_header_section',
		'settings' => 'kingcabs_header_layout',
		'choices' => array( 
			'headerone' => __('Header One', 'kingcabs'),
			'headertwo' => __('Header Two', 'kingcabs'),
    	)
    ));

	$wp_customize->add_setting(
		'kingcabs_header_phone',
		array(
			'sanitize_callback' => 'sanitize_text_field', // done
			'default'			=> '',
			'transport' 		=> 'postMessage'
		)
	);

	$wp_customize->add_control(
		'kingcabs_header_phone',
		array(
			'settings'		=> 'kingcabs_header_phone',
			'section'		=> 'kingcabs_header_section',
			'type'			=> 'text',
			'label'			=> __( 'Enter Phone Number', 'kingcabs' )
		)
	);
	
	$wp_customize->add_setting(
		'kingcabs_header_button_title',
		array(
			'sanitize_callback' => 'sanitize_text_field', // Done
			'default'			=> '',
			'transport'			=> 'postMessage',
		)
	);

	$wp_customize->add_control(
		'kingcabs_header_button_title',
		array(
			'settings'		=> 'kingcabs_header_button_title',
			'section'		=> 'kingcabs_header_section',
			'type'			=> 'text',
			'label'			=> __( 'Enter Button Text', 'kingcabs' )
		)
	);
	
	$wp_customize->add_setting(
		'kingcabs_header_button_url',
		array(
			'sanitize_callback' => 'esc_url_raw', // Done
			'default'			=> '',
			'transport'			=> 'postMessage'
		)
	);

	$wp_customize->add_control(
		'kingcabs_header_button_url',
		array(
			'settings'		=> 'kingcabs_header_button_url',
			'section'		=> 'kingcabs_header_section',
			'type'			=> 'url',
			'label'			=> esc_attr__( 'Enter Button URL', 'kingcabs' )
		)
	);

	$wp_customize->add_setting('kingcabs_header_link_to_social', array(
		'sanitize_callback' => 'kingcabs_sanitize_text'
	));
	
	$wp_customize->add_control(new KingCabs_Link_To_Social($wp_customize, 'kingcabs_header_link_to_social', array(
		'section' => 'kingcabs_header_section',
		'label' => __("Configure Social Settings", 'kingcabs'),
		'priority' => 100,
	)));

	/**
	 * SLIDER IMAGES SECTION
	*/	

	$wp_customize->add_section( 'kingcabs_image_carousel_section', array(
		'priority'		=> 2,
		'title'			=> __( "Slider", 'kingcabs' ),
		'description'	=> __( "Configure Main Banner Slider", 'kingcabs' ),
		'panel'			=> 'kingcabs_home_panel'	
	) );


	add_filter("kingcabs_image_carousel_nav_content_tab", function() {
		return [
			'kingcabs_slider_image_carousel_section_disable',
			'kingcabs_image_carousel_category',
			'kingcabs_image_carousel_number',
			'kingcabs_image_carousel_button_title',
		];
	});

	$wp_customize->add_setting(
		'kingcabs_slider_image_carousel_section_disable',
		array(
			'sanitize_callback' => 'sanitize_text_field', // Done
			'default' => 'off',
			'transport' => 'postMessage',
		)
	);

	$wp_customize->add_control(
		new Kingcabs_Switch_Control( $wp_customize, 'kingcabs_slider_image_carousel_section_disable',
			array(
				'settings'		=> 'kingcabs_slider_image_carousel_section_disable',
				'section'		=> 'kingcabs_image_carousel_section',
				'label'			=> __( "Enable Slider", 'kingcabs' ),
				'switch_label' 	=> array(
					'on' => __( 'Yes', 'kingcabs' ),
					'off' => __( 'No', 'kingcabs' )
				)	
			)
		)
	);

	$wp_customize->add_setting('kingcabs_image_carousel_category',array(
		'sanitize_callback' => 'absint', // Done
		'default' =>  1,
		'transport' => 'postMessage'
	) );

	$wp_customize->add_control(
		new Kingcabs_Theme_Customize_Dropdown_Taxonomies_Control( $wp_customize, 'kingcabs_image_carousel_category', array(
		'label' => __('Choose Category','kingcabs'),
		'section' => 'kingcabs_image_carousel_section',
		'settings' => 'kingcabs_image_carousel_category',
		'type'=> 'dropdown-taxonomies',
	) ) );

	$wp_customize->add_setting('kingcabs_image_carousel_number',array(
		'sanitize_callback' => 'absint', // Doone
		'default' =>  3,
		'transport' => 'postMessage'
	) );

	$wp_customize->add_control( 'kingcabs_image_carousel_number', array(
		'label' => __('Number of Sliders','kingcabs'),
		'section' => 'kingcabs_image_carousel_section',
		'settings' => 'kingcabs_image_carousel_number',
		'type'=> 'number',
	) );

	$wp_customize->selective_refresh->add_partial( 'kingcabs_slider_image_carousel', array(
		'settings' => array( 'kingcabs_slider_image_carousel_section_disable', 'kingcabs_image_carousel_category', 'kingcabs_image_carousel_number' ),
		'selector' => '.banner-slider',
		'container_inclusive' => true,
		'render_callback' => function () {
			return get_template_part( 'sections/section', 'carousel' );
		}
	));

	// Slider Button Title
	$wp_customize->add_setting( 'kingcabs_image_carousel_button_title', array(
		'sanitize_callback'	=> 'sanitize_text_field', // Done
		'default'			=> '',
		'transport' 		=> 'postMessage',
	));

	$wp_customize->add_control( 'kingcabs_image_carousel_button_title', array(
		'label'				=> __( 'Button Text', 'kingcabs' ),
		'section'			=> 'kingcabs_image_carousel_section',
		'settings'			=> 'kingcabs_image_carousel_button_title',
		'type'				=> 'text' 
	));

/**
 * Features Services Section
*/
	$wp_customize->add_section(
		'kingcabs_slider_button_section',
		array(
			'title' 			=> __( 'Features Services', 'kingcabs' ),
			'panel'  			=> 'kingcabs_home_panel',
			'priority'		=> 3,
		)
	);

	add_filter("kingcabs_slider_button_nav_content_tab", function() {

		return [
			'kingcabs_slider_button_page',
			'kingcabs_slider_button_section_disable',
			'kingcabs_slider_button_section_upgrade_text'
		];

	}, 100);

	$wp_customize->add_setting(
		'kingcabs_slider_button_section_disable',
		array(
			'sanitize_callback' => 'sanitize_text_field', // Done
			'default' => 'off', 
			'transport' => 'postMessage'
		)
	);

	$wp_customize->add_control(
		new Kingcabs_Switch_Control(
			$wp_customize,
			'kingcabs_slider_button_section_disable',
			array(
				'settings'		=> 'kingcabs_slider_button_section_disable',
				'section'		=> 'kingcabs_slider_button_section',
				'label'			=> __( 'Enable Features Services', 'kingcabs' ),
				'switch_label' 	=> array(
					'on' => __( 'Yes', 'kingcabs' ),
					'off' => __( 'No', 'kingcabs' )
				)	
			)
		)
	);

	$wp_customize->add_setting(
		'kingcabs_slider_button_page',
		array(
			'sanitize_callback' => 'kingcabs_sanitize_choices_array', // Done
			'transport' => 'postMessage',
		)
	);

	$wp_customize->add_control(
		new Kingcabs_Dropdown_Multiple_Chooser(
		$wp_customize,
		'kingcabs_slider_button_page',
		array(
			'settings'		=> 'kingcabs_slider_button_page',
			'section'		=> 'kingcabs_slider_button_section',
			'choices'		=> $kingcabs_page_choice,
			'label'			=> __( 'Select Features Pages', 'kingcabs' ),
 		)
	));

	$wp_customize->selective_refresh->add_partial( 'kingcabs_features_services', array(
		'settings' => array( 'kingcabs_slider_button_section_disable', 'kingcabs_slider_button_page' ),
		'selector' => '.kingcabs-slider-bottom',
		'container_inclusive' => true,
		'render_callback' => function () {
			return get_template_part( 'sections/section', 'features' );
		}
	));	

	$wp_customize->add_setting('kingcabs_slider_button_section_upgrade_text', array(
        'sanitize_callback' => 'kingcabs_sanitize_text'
    ));

    $wp_customize->add_control(new KingCabs_Upgrade_Text($wp_customize, 'kingcabs_slider_button_section_upgrade_text', array(
        'section' => 'kingcabs_slider_button_section',
        'label' => __('For more styles and settings,', 'kingcabs'),
        'choices' => array(
            __('More layout Option', 'kingcabs'),
            __('Customize Slider Button Text', 'kingcabs'),
            __('Advance Input Option', 'kingcabs'),
            __(' - Custom Input Field (Repeator)', 'kingcabs'),
            __(' - All Controls from customizer', 'kingcabs'),
            __(' - Content from page, and advance input', 'kingcabs'),
            __(' - And more...', 'kingcabs'),
        ),
        'priority' => 100
    )));


/**
 * About Services section
*/
	$wp_customize->add_section(
		'kingcabs_service_section',
		array(
			'title' 			=> __( 'About Services', 'kingcabs' ),
			'panel'     		=> 'kingcabs_home_panel',
			'priority'			=> 3,
		)
	);

	add_filter("kingcabs_service_nav_content_tab", function() {
		return [
			'kingcabs_service_page_disable', 
			'kingcabs_service_title',
			'kingcabs_service_sub_title',
			'kingcabs_service_page_title_icon',
			'kingcabs_service_left_bg',

			'kingcabs_service_header1',
			'kingcabs_service_header2',
			'kingcabs_service_header3',

			'kingcabs_service_page1', 
			'kingcabs_service_page_icon1', 
			'kingcabs_service_page2', 
			'kingcabs_service_page_icon2', 
			'kingcabs_service_page3', 
			'kingcabs_service_page_icon3',

			'kingcabs_service_section_upgrade_text',
		];
	});


	//ENABLE/DISABLE ABOUT Services Section
	$wp_customize->add_setting(
		'kingcabs_service_page_disable',
		array(
			'sanitize_callback' => 'sanitize_text_field', // Done
			'default' => 'off',
			'transport' => 'postMessage'
		)
	);

	$wp_customize->add_control(
		new Kingcabs_Switch_Control(
			$wp_customize,
			'kingcabs_service_page_disable',
			array(
				'settings'		=> 'kingcabs_service_page_disable',
				'section'		=> 'kingcabs_service_section',
				'label'			=> __( 'Enable Section', 'kingcabs' ),
				'switch_label' 	=> array(
					'on' => __( 'Yes', 'kingcabs' ),
					'off' => __( 'No', 'kingcabs' )
				)	
			)
		)
	);

	$wp_customize->add_setting(
		'kingcabs_service_title',
		array(
			'sanitize_callback' => 'sanitize_text_field', // Done
			'default'			=> '', 
			'transport'			=> 'postMessage'
		)
	);

	$wp_customize->add_control(
		'kingcabs_service_title',
		array(
			'settings'		=> 'kingcabs_service_title',
			'section'		=> 'kingcabs_service_section',
			'type'			=> 'text',
			'label'			=> __( "Title", "kingcabs" )
		)
	);

	$wp_customize->add_setting(
		'kingcabs_service_sub_title',
		array(
			'sanitize_callback' => 'sanitize_text_field', // Done
			'default'			=> '',
			'transport'			=> 'postMessage'
		)
	);

	$wp_customize->add_control(
		'kingcabs_service_sub_title',
		array(
			'settings'		=> 'kingcabs_service_sub_title',
			'section'		=> 'kingcabs_service_section',
			'type'			=> 'text',
			'label'			=> __( 'Sub Title', 'kingcabs' )
		)
	);

	$wp_customize->add_setting(
		'kingcabs_service_page_title_icon',
		array(
			'default'			=> 'fa fa-bell',
			'sanitize_callback' => 'sanitize_text_field', // Done
			'transport'			=> 'postMessage'
		)
	);

	$wp_customize->add_control(
		new Kingcabs_Fontawesome_Icon_Chooser(
			$wp_customize,
			'kingcabs_service_page_title_icon',
			array(
				'settings'		=> 'kingcabs_service_page_title_icon',
				'section'		=> 'kingcabs_service_section',
				'type'			=> 'icon',
				'label'			=> __( 'Feature Icon', 'kingcabs' )
			)
		)
	);

	$wp_customize->add_setting(
		'kingcabs_service_left_bg',
		array(
			'sanitize_callback' => 'esc_url_raw', // Done
			'default'			=> '',
			'transport'			=> 'postMessage',
		)
	);
 
	$wp_customize->add_control(
	    new WP_Customize_Image_Control(
	        $wp_customize,
	        'kingcabs_service_left_bg',
	        array(
	            'section' => 'kingcabs_service_section',
	            'settings' => 'kingcabs_service_left_bg',
	            'label'  => __( 'Upload Services Features Image', 'kingcabs' ),
	            'description' => __('Recommended Image Size: 770X650px', 'kingcabs')
	        )
	    )
	);
	
	for( $i = 1; $i < 4; $i++ ){

		$wp_customize->add_setting(
			'kingcabs_service_header'.$i,
			array(
				'default'			=> '',
				'sanitize_callback' => 'sanitize_text_field', // Done
				'transport'			=> 'postMessage'
			)
		);
		
		$wp_customize->add_control(
			new Kingcabs_Customize_Heading(
				$wp_customize,
				'kingcabs_service_header'.$i,
				array(
					'settings'		=> 'kingcabs_service_header'.$i,
					'section'		=> 'kingcabs_service_section',
					'label'			=> __( "Configure Service ", 'kingcabs' ) . $i
				)
			)
		);

		$wp_customize->add_setting(
			'kingcabs_service_page'.$i,
			array(
				'default'			=> '',
				'sanitize_callback' => 'absint', // Done
				'transport' 		=> 'postMessage'
			)
		);

		$wp_customize->add_control(
			'kingcabs_service_page'.$i,
			array(
				'settings'		=> 'kingcabs_service_page'.$i,
				'section'		=> 'kingcabs_service_section',
				'type'			=> 'dropdown-pages',
				'label'			=> __( 'Select Page', 'kingcabs' )
			)
		);

		$wp_customize->add_setting(
			'kingcabs_service_page_icon'.$i,
			array(
				'default'			=> 'fa-bell',
				'sanitize_callback' => 'sanitize_text_field', // Done
				'transport'			=> 'postMessage'
			)
		);

		$wp_customize->add_control(
			new Kingcabs_Fontawesome_Icon_Chooser(
				$wp_customize,
				'kingcabs_service_page_icon'.$i,
				array(
					'settings'		=> 'kingcabs_service_page_icon'.$i,
					'section'		=> 'kingcabs_service_section',
					'type'			=> 'icon',
					'label'			=> __( 'Choose Icon', 'kingcabs' )
				)
			)
		);
	}

	$wp_customize->selective_refresh->add_partial( 'kingcabs_about_services', array(
		'settings' => array( 'kingcabs_service_page_disable', 'kingcabs_service_page1', 'kingcabs_service_page_icon1', 'kingcabs_service_page2', 'kingcabs_service_page_icon2', 'kingcabs_service_page3', 'kingcabs_service_page_icon3' ),
		'selector' => '.kingcabs-service-section',
		'container_inclusive' => true,
		'render_callback' => function () {
			return get_template_part( 'sections/section', 'aboutservice' );
		}
	));

	$wp_customize->add_setting('kingcabs_service_section_upgrade_text', array(
        'sanitize_callback' => 'kingcabs_sanitize_text'
    ));

    $wp_customize->add_control(new KingCabs_Upgrade_Text($wp_customize, 'kingcabs_service_section_upgrade_text', array(
        'section' => 'kingcabs_service_section',
        'label' => __('For more styling,', 'kingcabs'),
        'choices' => array(
            __('Multiple Lyout', 'kingcabs'),
            __('Multiple Service Input', 'kingcabs'),
            __('Service from page and advance input', 'kingcabs'),
            __('And many more...', 'kingcabs'),
        ),
        'priority' => 100
    )));


/**
 * Main Services Section
*/
	$wp_customize->add_section(
		'kingcabs_services_section',
		array(
			'title'      => __( 'Main Services', 'kingcabs' ),
			'panel'      => 'kingcabs_home_panel',
			'priority'   => 4,
		)
	);


	add_filter("kingcabs_services_nav_content_tab", function() {
		return [
			'kingcabs_services_section_disable',
			'kingcabs_services_title',
			'kingcabs_services_sub_title',
			'kingcabs_services_icon_title',
			'kingcabs_services_page',
		];
	});


	$wp_customize->add_setting(
		'kingcabs_services_section_disable',
		array(
			'sanitize_callback' => 'sanitize_text_field', // Done
			'default' => 'on',
			'transport' => 'postMessage'
		)
	);

	$wp_customize->add_control(
		new Kingcabs_Switch_Control(
			$wp_customize,
			'kingcabs_services_section_disable',
			array(
				'settings'		=> 'kingcabs_services_section_disable',
				'section'		=> 'kingcabs_services_section',
				'label'			=> __( 'Enable Section', 'kingcabs' ),
				'switch_label' 	=> array(
					'on' => __( 'Yes', 'kingcabs' ),
					'off' => __( 'No', 'kingcabs' )
				)	
			)
		)
	);

	$wp_customize->add_setting(
		'kingcabs_services_title',
		array(
			'sanitize_callback' => 'sanitize_text_field', // Done
			'default'			=> '',
			'transport' 		=> 'postMessage'
		)
	);

	$wp_customize->add_control(
		'kingcabs_services_title',
		array(
			'settings'		=> 'kingcabs_services_title',
			'section'		=> 'kingcabs_services_section',
			'type'			=> 'text',
			'label'			=> __( 'Title', 'kingcabs' )
		)
	);

	$wp_customize->add_setting(
		'kingcabs_services_sub_title',
		array(
			'sanitize_callback' => 'sanitize_text_field', // Done
			'default'			=> '',
			'transport'			=> 'postMessage'
		)
	);

	$wp_customize->add_control(
		'kingcabs_services_sub_title',
		array(
			'settings'		=> 'kingcabs_services_sub_title',
			'section'		=> 'kingcabs_services_section',
			'type'			=> 'text',
			'label'			=> __( 'Sub Title', 'kingcabs' )
		)
	);

	$wp_customize->add_setting(
		'kingcabs_services_icon_title',
		array(
			'default'			=> 'fa fa-automobile',
			'sanitize_callback' => 'sanitize_text_field', // Done
			'transport'			=> 'postMessage'
		)
	);

	$wp_customize->add_control(
		new Kingcabs_Fontawesome_Icon_Chooser(
			$wp_customize,
			'kingcabs_services_icon_title',
			array(
				'settings'		=> 'kingcabs_services_icon_title',
				'section'		=> 'kingcabs_services_section',
				'type'			=> 'icon',
				'label'			=> __( 'Feature Icon', 'kingcabs' )
			)
		)
	);


	$wp_customize->add_setting(
		'kingcabs_services_page',
		array(
			'sanitize_callback' => 'kingcabs_sanitize_choices_array', // Done
			'transport'			=> 'postMessage'
		)
	);

	$wp_customize->add_control(
		new Kingcabs_Dropdown_Multiple_Chooser(
		$wp_customize,
		'kingcabs_services_page',
		array(
			'settings'		=> 'kingcabs_services_page',
			'section'		=> 'kingcabs_services_section',
			'choices'		=> $kingcabs_page_choice,
			'label'			=> __( 'Select Pages', 'kingcabs' )
 		)
	));

	$wp_customize->selective_refresh->add_partial( 'kingcabs_main_services', array(
		'settings' => array( 'kingcabs_services_section_disable', 'kingcabs_services_page' ),
		'selector' => '.kingcabs-main-services',
		'container_inclusive' => true,
		'render_callback' => function () {
			if( get_theme_mod( 'kingcabs_services_section_disable', 'on' ) === 'on' ) {
				return get_template_part( 'sections/section', 'mainservices' );
			}
		}
	));

/**
 * Counter Section Area
*/
	$wp_customize->add_section(
		'kingcabs_counter_section',
		array(
			'title' => __( 'Counter', 'kingcabs' ),
			'panel'	=> 'kingcabs_home_panel',
			'priority'		=> 4,
		)
	);

	add_filter("kingcabs_counter_nav_content_tab", function() {
		return [
			'kingcabs_counter_section_disable',
			'kingcabs_counter_title',
			'kingcabs_counter_sub_title',
			'kingcabs_counter_bg',
			'kingcabs_counter_icon_title',
			'kingcabs_counter_style',
			'kingcabs_counter_section_upgrade_text',

			'kingcabs_counter_heading1', 
			'kingcabs_counter_heading2', 
			'kingcabs_counter_heading3', 
			'kingcabs_counter_heading4', 
			'kingcabs_counter_title1',
			'kingcabs_counter_count1', 
			'kingcabs_counter_suffix1', 
			'kingcabs_counter_icon1', 
			'kingcabs_counter_title2', 
			'kingcabs_counter_count2', 
			'kingcabs_counter_suffix2', 
			'kingcabs_counter_icon2', 
			'kingcabs_counter_title3', 
			'kingcabs_counter_count3', 
			'kingcabs_counter_suffix3', 
			'kingcabs_counter_icon3',
			'kingcabs_counter_title4', 
			'kingcabs_counter_count4', 
			'kingcabs_counter_suffix4', 
			'kingcabs_counter_icon4'
		];
	});

	$wp_customize->add_setting(
		'kingcabs_counter_section_disable',
		array(
			'sanitize_callback' => 'sanitize_text_field', // Done
			'default' => 'off', 
			'transport' => 'postMessage'
		)
	);

	$wp_customize->add_control(
		new Kingcabs_Switch_Control(
			$wp_customize,
			'kingcabs_counter_section_disable',
			array(
				'settings'		=> 'kingcabs_counter_section_disable',
				'section'		=> 'kingcabs_counter_section',
				'label'			=> __( 'Enable Counter', 'kingcabs' ),
				'switch_label' 	=> array(
					'on' => __( 'Yes', 'kingcabs' ),
					'off' => __( 'No', 'kingcabs' )
				)	
			)
		)
	);

	$wp_customize->add_setting(
		'kingcabs_counter_title',
		array(
			'sanitize_callback' => 'sanitize_text_field', // Done
			'default'			=> '',
			'transport' 		=> 'postMessage'
		)
	);

	$wp_customize->add_control(
		'kingcabs_counter_title',
		array(
			'settings'		=> 'kingcabs_counter_title',
			'section'		=> 'kingcabs_counter_section',
			'type'			=> 'text',
			'label'			=> __( 'Title', 'kingcabs' )
		)
	);

	$wp_customize->add_setting(
		'kingcabs_counter_sub_title',
		array(
			'sanitize_callback' => 'sanitize_text_field', // Done
			'default'			=> '',
			'transport'			=> 'postMessage'
		)
	);

	$wp_customize->add_control(
		'kingcabs_counter_sub_title',
		array(
			'settings'		=> 'kingcabs_counter_sub_title',
			'section'		=> 'kingcabs_counter_section',
			'type'			=> 'text',
			'label'			=> __( 'Sub Title', 'kingcabs' )
		)
	);

	$wp_customize->add_setting(
		'kingcabs_counter_bg',
		array(
			'sanitize_callback' => 'esc_url_raw',  // Done
			'default'			=> '',
			'transport'			=> 'postMessage'
		)
	);

	$wp_customize->add_control(
	    new WP_Customize_Image_Control(
	        $wp_customize,
	        'kingcabs_counter_bg',
	        array(
	            'label' => __( 'Upload Image', 'kingcabs' ),
	            'section' => 'kingcabs_counter_section',
	            'settings' => 'kingcabs_counter_bg',
	            'label' => __('Background Image','kingcabs'),
	            'description' => __('Recommended Image Size: 1800 X 400PX', 'kingcabs')
	        )
	    )
	);


	$wp_customize->add_setting(
		'kingcabs_counter_icon_title',
		array(
			'default'			=> 'fa fa-bell',
			'sanitize_callback' => 'sanitize_text_field', // Done
			'transport'			=> 'postMessage'
		)
	);

	$wp_customize->add_control(
		new Kingcabs_Fontawesome_Icon_Chooser(
			$wp_customize,
			'kingcabs_counter_icon_title',
			array(
				'settings'		=> 'kingcabs_counter_icon_title',
				'section'		=> 'kingcabs_counter_section',
				'type'			=> 'icon',
				'label'			=> __( 'Feature Icon', 'kingcabs' )
			)
		)
	);

	// Counter Section Area
	for( $i = 1; $i < 5; $i++ ){

		$wp_customize->add_setting(
			'kingcabs_counter_heading'.$i,
			array(
				'sanitize_callback' => 'sanitize_text_field', // Done
				'transport'	=> 'postMessage'
			)
		);

		$wp_customize->add_control(
			new Kingcabs_Customize_Heading(
				$wp_customize,
				'kingcabs_counter_heading'.$i,
				array(
					'settings'		=> 'kingcabs_counter_heading'.$i,
					'section'		=> 'kingcabs_counter_section',
					'label'			=> __( "Counter Item ", 'kingcabs' ) . $i,
				)
			)
		);

		$wp_customize->add_setting(
			'kingcabs_counter_title'.$i,
			array(
				'sanitize_callback' => 'sanitize_text_field', // Done
				'transport' => 'postMessage'
			)
		);

		$wp_customize->add_control(
			'kingcabs_counter_title'.$i,
			array(
				'settings'		=> 'kingcabs_counter_title'.$i,
				'section'		=> 'kingcabs_counter_section',
				'type'			=> 'text',
				'label'			=> __( 'Title', 'kingcabs' )
			)
		);

		$wp_customize->add_setting(
			'kingcabs_counter_count'.$i,
			array(
				'sanitize_callback' => 'absint', // Done
				'transport' => 'postMessage'
			)
		);

		$wp_customize->add_control(
			'kingcabs_counter_count'.$i,
			array(
				'settings'		=> 'kingcabs_counter_count'.$i,
				'section'		=> 'kingcabs_counter_section',
				'type'			=> 'number',
				'label'			=> __( 'Number', 'kingcabs' )
			)
		);

		$wp_customize->add_setting(
			'kingcabs_counter_suffix'.$i,
			array(
				'sanitize_callback' => 'sanitize_text_field', // Done
				'transport' => 'postMessage'
			)
		);

		$wp_customize->add_control(
			'kingcabs_counter_suffix'.$i,
			array(
				'settings'		=> 'kingcabs_counter_suffix'.$i,
				'section'		=> 'kingcabs_counter_section',
				'type'			=> 'text',
				'label'			=> __( 'Suffix', 'kingcabs' )
			)
		);

		$wp_customize->add_setting(
			'kingcabs_counter_icon'.$i,
			array(
				'default'			=> 'fa fa-bell', 
				'sanitize_callback' => 'sanitize_text_field', // Done
				'transport' 		=> 'postMessage'
			)
		);

		$wp_customize->add_control(
			new Kingcabs_Fontawesome_Icon_Chooser(
				$wp_customize,
				'kingcabs_counter_icon'.$i,
				array(
					'settings'		=> 'kingcabs_counter_icon'.$i,
					'section'		=> 'kingcabs_counter_section',
					'type'			=> 'icon',
					'label'			=> __( 'Icon', 'kingcabs' )
				)
			)
		);
	}

	$wp_customize->selective_refresh->add_partial( 'kingcabs_counter', array(
		'settings' => array( 'kingcabs_counter_section_disable', 'kingcabs_counter_title1', 'kingcabs_counter_count1', 'kingcabs_counter_suffix1', 'kingcabs_counter_icon1', 'kingcabs_counter_title2', 'kingcabs_counter_count2', 'kingcabs_counter_suffix2', 'kingcabs_counter_icon2', 'kingcabs_counter_title3', 'kingcabs_counter_count3', 'kingcabs_counter_suffix3', 'kingcabs_counter_icon3', 'kingcabs_counter_title4', 'kingcabs_counter_count4', 'kingcabs_counter_suffix4', 'kingcabs_counter_icon4' ),
		'selector' => '.kingcabs-counter',
		'container_inclusive' => true,
		'render_callback' => function () {
			return get_template_part( 'sections/section', 'counter' );
		}
	));	

	$wp_customize->add_setting('kingcabs_counter_style', array(
		'default' => 'style1',
		'sanitize_callback' => 'sanitize_text_field',  //done
		'transport'	=> 'postMessage'
    ));

    $wp_customize->add_control('kingcabs_counter_style', array(
		'type' => 'select',
		'label' => __('Select Style', 'kingcabs'),
		'section' => 'kingcabs_counter_section',
		'settings' => 'kingcabs_counter_style',
		'choices' => array( 
			'style1' => __('Style 1', 'kingcabs'),
			'style2' => __('Style 2', 'kingcabs'),
    	)
    ));

    $wp_customize->add_setting('kingcabs_counter_section_upgrade_text', array(
        'sanitize_callback' => 'kingcabs_sanitize_text'
    ));

/**
 * Fleet Section Settings Area
*/
	$wp_customize->add_section(
		'kingcabs_fleet_section',
		array(
			'title' 			=> __( 'Fleet', 'kingcabs' ),
			'panel'				=> 'kingcabs_home_panel',
			'priority'		=> 5,
		)
	);

	add_filter("kingcabs_fleet_nav_content_tab", function() {
		return [
			'kingcabs_fleet_section_disable',

			'kingcabs_fleet_title',
			'kingcabs_fleet_sub_title',
			'kingcabs_fleet_icon_title',
			'kingcabs_portfolio_area_term_id',
			'kingcabs_fleet_button_title',
			'kingcabs_fleet_button_url',
			'kingcabs_fleet_section_upgrade_text',
		];
	});


	$wp_customize->add_setting(
		'kingcabs_fleet_section_disable',
		array(
			'sanitize_callback' => 'sanitize_text_field', // Done
			'default' => 'off',
			'transport' => 'postMessage'
		)
	);

	$wp_customize->add_control(
		new Kingcabs_Switch_Control(
			$wp_customize,
			'kingcabs_fleet_section_disable',
			array(
				'settings'		=> 'kingcabs_fleet_section_disable',
				'section'		=> 'kingcabs_fleet_section',
				'label'			=> __( 'Enable Section', 'kingcabs' ),
				'switch_label' 	=> array(
					'on' => __( 'Yes', 'kingcabs' ),
					'off' => __( 'No', 'kingcabs' )
				)	
			)
		)
	);

	$wp_customize->add_setting(
		'kingcabs_fleet_title',
		array(
			'sanitize_callback' => 'sanitize_text_field', // Done
			'default'			=> '',
			'transport'			=> 'postMessage'
		)
	);

	$wp_customize->add_control(
		'kingcabs_fleet_title',
		array(
			'settings'		=> 'kingcabs_fleet_title',
			'section'		=> 'kingcabs_fleet_section',
			'type'			=> 'text',
			'label'			=> __( 'Title', 'kingcabs' )
		)
	);

	$wp_customize->add_setting(
		'kingcabs_fleet_sub_title',
		array(
			'sanitize_callback' => 'sanitize_text_field', // Done
			'default'			=> '',
			'transport' 		=> 'postMessage'
		)
	);

	$wp_customize->add_control(
		'kingcabs_fleet_sub_title',
		array(
			'settings'		=> 'kingcabs_fleet_sub_title',
			'section'		=> 'kingcabs_fleet_section',
			'type'			=> 'text',
			'label'			=> __( 'Sub Title', 'kingcabs' )
		)
	);

	$wp_customize->add_setting(
		'kingcabs_fleet_icon_title',
		array(
			'default'			=> 'fa fa-bell',
			'sanitize_callback' => 'sanitize_text_field', // Done
			'transport'			=> 'postMessage'
		)
	);

	$wp_customize->add_control(
		new Kingcabs_Fontawesome_Icon_Chooser(
			$wp_customize,
			'kingcabs_fleet_icon_title',
			array(
				'settings'		=> 'kingcabs_fleet_icon_title',
				'section'		=> 'kingcabs_fleet_section',
				'type'			=> 'icon',
				'label'			=> __( 'Feature Icon', 'kingcabs' )
			)
		)
	);

	$wp_customize->add_setting( 'kingcabs_portfolio_area_term_id', array(
		'default'			=> '',
		'sanitize_callback' => 'sanitize_text_field',
		'transport'			=> 'postMessage'
	) );
	
	$wp_customize->add_control( new Kingcabs_Customize_Control_Checkbox_Multiple( $wp_customize, 'kingcabs_portfolio_area_term_id', array(
        'label' => __( 'Select Cateogries', 'kingcabs' ),
        'section' => 'kingcabs_fleet_section',
        'settings' => 'kingcabs_portfolio_area_term_id',
        'choices' => $cat_lists
    ) ) );

	$wp_customize->selective_refresh->add_partial( 'kingcabs_fleet', array(
		'settings' => array( 'kingcabs_fleet_section_disable', 'kingcabs_portfolio_area_term_id' ),
		'selector' => '.kingcabs-fleet',
		'container_inclusive' => true,
		'render_callback' => function () {
			return get_template_part( 'sections/section', 'fleet' );
		}
	));

    $wp_customize->add_setting( 'kingcabs_fleet_button_title', array(
		'sanitize_callback' => 'sanitize_text_field', // Done
		'default'			=> '',
		'transport'			=> 'postMessage'
	));

	$wp_customize->add_control( 'kingcabs_fleet_button_title', array(
		'settings'		=> 'kingcabs_fleet_button_title',
		'section'		=> 'kingcabs_fleet_section',
		'type'			=> 'text',
		'label'			=> __( 'Button Text', 'kingcabs' )
	));

	$wp_customize->add_setting( 'kingcabs_fleet_button_url', array(
		'sanitize_callback' => 'esc_url_raw', // Done
		'default'			=> '',
		'transport'			=> 'postMessage'
	));

	$wp_customize->add_control( 'kingcabs_fleet_button_url', array(
		'settings'		=> 'kingcabs_fleet_button_url',
		'section'		=> 'kingcabs_fleet_section',
		'type'			=> 'url',
		'label'			=> esc_attr__( 'Button URL', 'kingcabs' )
	));

	$wp_customize->add_setting('kingcabs_fleet_section_upgrade_text', array(
        'sanitize_callback' => 'kingcabs_sanitize_text'
    ));

    $wp_customize->add_control(new KingCabs_Upgrade_Text($wp_customize, 'kingcabs_fleet_section_upgrade_text', array(
        'section' => 'kingcabs_fleet_section',
        'label' => __('For more settings and controls,', 'kingcabs'),
        'choices' => array(
            __('Choose from two different layouts', 'kingcabs'),
            __('Control over display number of fleets', 'kingcabs'),
            __('Enable/Disable Fleet Special Rate', 'kingcabs'),
            __('Background - Margin - Padding Controls', 'kingcabs'),
        ),
        'priority' => 100
    )));

/**
 * Testimoial Section
*/
	$wp_customize->add_section(
		'kingcabs_testimonial_section',
		array(
			'title' => __( 'Testimonials', 'kingcabs' ),
			'panel'  => 'kingcabs_home_panel',
			'priority'		=> 6,
		)
	);

	add_filter("kingcabs_testimonial_nav_content_tab", function() {
		return [
			'kingcabs_testimonial_section_disable',
			'kingcabs_testimonial_title',
			'kingcabs_testimonial_sub_title',
			'kingcabs_testimonial_icon_title',
			'kingcabs_testimonial_section_layout',
			'kingcabs_testimonial_page',
			'kingcabs_testimonial_section_upgrade_text',
		];
	});

	$wp_customize->add_setting(
		'kingcabs_testimonial_section_disable',
		array(
			'sanitize_callback' => 'sanitize_text_field', // Done
			'default' => 'off',
			'transport'		=> 'postMessage'
		)
	);

	$wp_customize->add_control(
		new Kingcabs_Switch_Control(
			$wp_customize,
			'kingcabs_testimonial_section_disable',
			array(
				'settings'		=> 'kingcabs_testimonial_section_disable',
				'section'		=> 'kingcabs_testimonial_section',
				'label'			=> __( 'Enable Section', 'kingcabs' ),
				'switch_label' 	=> array(
					'on' => __( 'Yes', 'kingcabs' ),
					'off' => __( 'No', 'kingcabs' )
				)	
			)
		)
	);

	$wp_customize->add_setting(
		'kingcabs_testimonial_title',
		array(
			'sanitize_callback' => 'sanitize_text_field', // Done
			'default'			=> '',
			'transport'			=> 'postMessage'
		)
	);

	$wp_customize->add_control(
		'kingcabs_testimonial_title',
		array(
			'settings'		=> 'kingcabs_testimonial_title',
			'section'		=> 'kingcabs_testimonial_section',
			'type'			=> 'text',
			'label'			=> __( 'Title', 'kingcabs' )
		)
	);

	$wp_customize->add_setting(
		'kingcabs_testimonial_sub_title',
		array(
			'sanitize_callback' => 'sanitize_text_field', // Done
			'default'			=> '',
			'transport'			=> 'postMessage'
		)
	);
	$wp_customize->add_control(
		'kingcabs_testimonial_sub_title',
		array(
			'settings'		=> 'kingcabs_testimonial_sub_title',
			'section'		=> 'kingcabs_testimonial_section',
			'type'			=> 'text',
			'label'			=> __( 'Sub Title', 'kingcabs' )
		)
	);

	$wp_customize->add_setting(
		'kingcabs_testimonial_icon_title',
		array(
			'default'			=> 'fa fa-bell',
			'sanitize_callback' => 'sanitize_text_field', // Done
			'transport'			=> 'postMessage'
		)
	);

	$wp_customize->add_control(
		new Kingcabs_Fontawesome_Icon_Chooser(
			$wp_customize,
			'kingcabs_testimonial_icon_title',
			array(
				'settings'		=> 'kingcabs_testimonial_icon_title',
				'section'		=> 'kingcabs_testimonial_section',
				'type'			=> 'icon',
				'label'			=> __( 'Feature Icon', 'kingcabs' )
			)
		)
	);

	/** layout */
	$wp_customize->add_setting('kingcabs_testimonial_section_layout', array(
        'sanitize_callback' => 'kingcabs_sanitize_text',
		'default' => 'layoutone',
		'transport'	=> 'postMessage'
    ));

    $wp_customize->add_control('kingcabs_testimonial_section_layout', array(
        'section' => 'kingcabs_testimonial_section',
		'type' => 'select',
        'label' => __('Layout', 'kingcabs'),
        'choices' => array( 
			'layoutone' => __('Layout One', 'kingcabs'),
          	'layouttwo' => __('Layout Two', 'kingcabs'),
    	)
    ));

	$wp_customize->add_setting(
		'kingcabs_testimonial_page',
		array(
			'sanitize_callback' => 'kingcabs_sanitize_choices_array', // Done
			'transport'			=> 'postMessage'
		)
	);

	$wp_customize->add_control(
		new Kingcabs_Dropdown_Multiple_Chooser(
		$wp_customize,
		'kingcabs_testimonial_page',
		array(
			'settings'		=> 'kingcabs_testimonial_page',
			'section'		=> 'kingcabs_testimonial_section',
			'choices'		=> $kingcabs_page_choice,
			'label'			=> __( 'Select Pages', 'kingcabs' )
 		)
	));

	$wp_customize->selective_refresh->add_partial( 'kingcabs_testimonial', array(
		'settings' => array( 'kingcabs_testimonial_section_disable', 'kingcabs_testimonial_page' ),
		'selector' => '.kingcabs-testimonials',
		'container_inclusive' => true,
		'render_callback' => function () {
			return get_template_part( 'sections/section', 'testimonial' );
		}
	));

	$wp_customize->add_setting('kingcabs_testimonial_section_upgrade_text', array(
        'sanitize_callback' => 'kingcabs_sanitize_text'
    ));

    $wp_customize->add_control(new KingCabs_Upgrade_Text($wp_customize, 'kingcabs_testimonial_section_upgrade_text', array(
        'section' => 'kingcabs_testimonial_section',
        'label' => __('For more settings,', 'kingcabs'),
        'choices' => array(
            __('Switch Between Two Different Layouts', 'kingcabs'),
            __('Advnace Input Options', 'kingcabs'),
            __('All Controls from customizer', 'kingcabs'),
        ),
        'priority' => 100
    )));

/**
 * Drive Section Settings Area
*/
	$wp_customize->add_section(
		'kingcabs_driver_section',
		array(
			'title'			=> __( 'Drivers', 'kingcabs' ),
			'panel'         => 'kingcabs_home_panel',
			'priority'		=> 7,
		)
	);

	add_filter("kingcabs_driver_nav_content_tab", function() {
		return [
			'kingcabs_driver_section_disable',
			'kingcabs_driver_title',
			'kingcabs_driver_sub_title',
			'kingcabs_driver_page_title_icon',
			'kingcabs_driver_column',
			'kingcabs_driver_section_upgrade_text',

			'kingcabs_driver_heading1',
			'kingcabs_driver_heading2',
			'kingcabs_driver_heading3',
			'kingcabs_driver_heading4',

			'kingcabs_driver_page1', 
			'kingcabs_driver_designation1', 
			'kingcabs_driver_page2', 
			'kingcabs_driver_designation2',
			'kingcabs_driver_page3', 
			'kingcabs_driver_designation3', 
			'kingcabs_driver_page4', 
			'kingcabs_driver_designation4'
		

		];
	});


	$wp_customize->add_setting(
		'kingcabs_driver_section_disable',
		array(
			'sanitize_callback' => 'sanitize_text_field', // Done
			'default' => 'off',
			'transport' => 'postMessage'
		)
	);

	$wp_customize->add_control(
		new Kingcabs_Switch_Control(
			$wp_customize,
			'kingcabs_driver_section_disable',
			array(
				'settings'		=> 'kingcabs_driver_section_disable',
				'section'		=> 'kingcabs_driver_section',
				'label'			=> __( 'Enable Section', 'kingcabs' ),
				'switch_label' 	=> array(
					'on' => __( 'Yes', 'kingcabs' ),
					'off' => __( 'No', 'kingcabs' )
				)	
			)
		)
	);

	$wp_customize->add_setting(
		'kingcabs_driver_title',
		array(
			'sanitize_callback' => 'sanitize_text_field', // Done
			'default'			=> '',
			'transport' 		=> 'postMessage'
		)
	);

	$wp_customize->add_control(
		'kingcabs_driver_title',
		array(
			'settings'		=> 'kingcabs_driver_title',
			'section'		=> 'kingcabs_driver_section',
			'type'			=> 'text',
			'label'			=> __( 'Title', 'kingcabs' )
		)
	);

	$wp_customize->add_setting(
		'kingcabs_driver_sub_title',
		array(
			'sanitize_callback' => 'sanitize_text_field',  // Done
			'default'			=> '',
			'transport'			=> 'postMessage'
		)
	);

	$wp_customize->add_control(
		'kingcabs_driver_sub_title',
		array(
			'settings'		=> 'kingcabs_driver_sub_title',  // Done
			'section'		=> 'kingcabs_driver_section',
			'type'			=> 'text',
			'label'			=> __( 'Sub Title', 'kingcabs' )
		)
	);

	$wp_customize->add_setting(
		'kingcabs_driver_page_title_icon',
		array(
			'default'			=> 'fa-bell',
			'sanitize_callback' => 'sanitize_text_field',  // Done
			'transport'			=> 'postMessage'
		)
	);

	$wp_customize->add_control(
		new Kingcabs_Fontawesome_Icon_Chooser(
			$wp_customize,
			'kingcabs_driver_page_title_icon',
			array(
				'settings'		=> 'kingcabs_driver_page_title_icon',
				'section'		=> 'kingcabs_driver_section',
				'type'			=> 'icon',
				'label'			=> __( 'Feature Icon', 'kingcabs' )
			)
		)
	);

	$wp_customize->add_setting('kingcabs_driver_column', array(
		'default' => 3,
		'sanitize_callback' => 'kingcabs_driver_column',  //done
		'transport'	=> 'postMessage'
    ));

    $wp_customize->add_control('kingcabs_driver_column', array(
		'type' => 'select',
		'label' => __('Number of Columns', 'kingcabs'),
		'section' => 'kingcabs_driver_section',
		'settings' => 'kingcabs_driver_column',
		'choices' => array( 
			'1' => '1',
          	'2' => '2',  
			'3' => '3', 
			'4' => '4',
    	)
    ));
    
	for( $i = 1; $i < 5; $i++ ){
		$wp_customize->add_setting(
			'kingcabs_driver_heading'.$i,
			array(
				'sanitize_callback' => 'sanitize_text_field',  // Done
			)
		);

		$wp_customize->add_control(
			new Kingcabs_Customize_Heading(
				$wp_customize,
				'kingcabs_driver_heading'.$i,
				array(
					'settings'		=> 'kingcabs_driver_heading'.$i,
					'section'		=> 'kingcabs_driver_section',
					'label'			=> __( "Configure Driver ", 'kingcabs' ). $i,
				)
			)
		);

		$wp_customize->add_setting(
			'kingcabs_driver_page'.$i,
			array(
				'sanitize_callback' => 'absint', // Done
				'transport' => 'postMessage'
			)
		);

		$wp_customize->add_control(
			'kingcabs_driver_page'.$i,
			array(
				'settings'		=> 'kingcabs_driver_page'.$i,
				'section'		=> 'kingcabs_driver_section',
				'type'			=> 'dropdown-pages',
				'label'			=> __( 'Select Page', 'kingcabs' )
			)
		);

		$wp_customize->add_setting(
			'kingcabs_driver_designation'.$i,
			array(
				'sanitize_callback' => 'sanitize_text_field',  // Done
				'transport' => 'postMessage'
			)
		);

		$wp_customize->add_control(
			'kingcabs_driver_designation'.$i,
			array(
				'settings'		=> 'kingcabs_driver_designation'.$i,
				'section'		=> 'kingcabs_driver_section',
				'type'			=> 'text',
				'label'			=> __( 'Designation', 'kingcabs' )
			)
		);
		
	}

	$wp_customize->selective_refresh->add_partial( 'kingcabs_driver', array(
		'settings' => array( 'kingcabs_driver_section_disable', 'kingcabs_driver_column', 'kingcabs_driver_page1', 'kingcabs_driver_designation1', 'kingcabs_driver_page2', 'kingcabs_driver_designation2', 'kingcabs_driver_page3', 'kingcabs_driver_designation3', 'kingcabs_driver_page4', 'kingcabs_driver_designation4' ),
		'selector' => '.kingcabs-team',
		'container_inclusive' => true,
		'render_callback' => function () {
			return get_template_part( 'sections/section', 'driver' );
		}
	));

	$wp_customize->selective_refresh->add_partial('kingcabs_driver_page1', array(
		'selector' => '.testimonials .section-title .team-item',
		'container_inclusive' => true
	));

	$wp_customize->add_setting('kingcabs_driver_section_upgrade_text', array(
        'sanitize_callback' => 'kingcabs_sanitize_text'
    ));

    $wp_customize->add_control(new KingCabs_Upgrade_Text($wp_customize, 'kingcabs_driver_section_upgrade_text', array(
        'section' => 'kingcabs_driver_section',
        'label' => __('For more settings,', 'kingcabs'),
        'choices' => array(
            __('Switch Between Layout One and Layout Two', 'kingcabs'),
            __('More Driver Areas', 'kingcabs'),
            __('Driver areas include social links', 'kingcabs'),
            __('Advance inputs', 'kingcabs'),
            __('All controls from customizer', 'kingcabs'),
            __('And more...', 'kingcabs'),
        ),
        'priority' => 100
    )));

	/*
	* Call To Aciton Section 
	*/
	$wp_customize->add_section(
		'kingcabs_call_to_action_section',
		array(
			'title' => __( 'Call To Action', 'kingcabs' ),
			'panel' => 'kingcabs_home_panel',
			'priority'		=> 8,
		)
	);

	add_filter("kingcabs_call_to_action_nav_content_tab", function() {
		return [
			'kingcabs_call_to_action_page_disable',
			'kingcabs_call_to_action_page',
			'kingcabs_call_to_action_button_text',
			'kingcabs_call_to_action_button_number',
			'kingcabs_call_to_action_section_upgrade_text',
		];
	});


	$wp_customize->add_setting(
		'kingcabs_call_to_action_page_disable',
		array(
			'sanitize_callback' => 'sanitize_text_field', // Done
			'default' => 'off',
			'transport' => 'postMessage'
		)
	);

	$wp_customize->add_control(
		new Kingcabs_Switch_Control(
			$wp_customize,
			'kingcabs_call_to_action_page_disable',
			array(
				'settings'		=> 'kingcabs_call_to_action_page_disable',
				'section'		=> 'kingcabs_call_to_action_section',
				'label'			=> __( 'Enable Section', 'kingcabs' ),
				'switch_label' 	=> array(
					'on' => __( 'Yes', 'kingcabs' ),
					'off' => __( 'No', 'kingcabs' )
				)	
			)
		)
	);

	$wp_customize->add_setting(
		'kingcabs_call_to_action_page',
		array(
			'sanitize_callback' => 'absint',  // Done
			'transport' => 'postMessage'
		)
	);

	$wp_customize->add_control(
		'kingcabs_call_to_action_page',
		array(
			'settings'		=> 'kingcabs_call_to_action_page',
			'section'		=> 'kingcabs_call_to_action_section',
			'type'			=> 'dropdown-pages',
			'label'			=> __( 'Select Page', 'kingcabs' ),	
		)
	);

	$wp_customize->selective_refresh->add_partial( 'kingcabs_call_to_action', array(
		'settings' => array( 'kingcabs_call_to_action_page_disable', 'kingcabs_call_to_action_page' ),
		'selector' => '.kingcabs-call-us', 
		'container_inclusive' => true,
		'render_callback' => function () {
			return get_template_part( 'sections/section', 'cta' );
		}
	));

	$wp_customize->add_setting(
		'kingcabs_call_to_action_button_text',
		array(
			'sanitize_callback' => 'sanitize_text_field',  // Done
			'default'			=> '',
			'transport'			=> 'postMessage'
		)
	);

	$wp_customize->add_control(
		'kingcabs_call_to_action_button_text',
		array(
			'settings'		=> 'kingcabs_call_to_action_button_text',
			'section'		=> 'kingcabs_call_to_action_section',
			'type'			=> 'text',
			'label'			=> __( 'Button Text', 'kingcabs' )
		)
	);

	$wp_customize->add_setting(
		'kingcabs_call_to_action_button_number',
		array(
			'sanitize_callback' => 'sanitize_text_field',  // Done
			'default'			=> '',
			'transport'			=> 'postMessage'
		)
	);

	$wp_customize->add_control(
		'kingcabs_call_to_action_button_number',
		array(
			'settings'		=> 'kingcabs_call_to_action_button_number',
			'section'		=> 'kingcabs_call_to_action_section',
			'type'			=> 'text',
			'label'			=> __( 'Number', 'kingcabs' )
		)
	);

	$wp_customize->add_setting('kingcabs_call_to_action_section_upgrade_text', array(
        'sanitize_callback' => 'kingcabs_sanitize_text'
    ));

    $wp_customize->add_control(new KingCabs_Upgrade_Text($wp_customize, 'kingcabs_call_to_action_section_upgrade_text', array(
        'section' => 'kingcabs_call_to_action_section',
        'label' => __('For more layouts and controls,', 'kingcabs'),
        'choices' => array(
            __('Choose from two different layouts', 'kingcabs'),
            __('Call to action features image', 'kingcabs'),
        ),
        'priority' => 100
    )));

/**
 * Client/Brand Logo Secton 
*/
	$wp_customize->add_Section(
		'kingcabs_client_logo_section',
		array(
			'title' => __( 'Client/Brand Logo', 'kingcabs' ),
			'panel'	=> 'kingcabs_home_panel'
		)
	);

	add_filter("kingcabs_client_logo_nav_content_tab", function() {
		return [
			'kingcabs_client_logo_section_disable',
			'kingcabs_client_logo_image',
			'kingcabs_client_logo_section_upgrade_text',
		];
	});


	$wp_customize->add_setting(
		'kingcabs_client_logo_section_disable',
		array(
			'sanitize_callback' => 'sanitize_text_field', // Done
			'default' => 'off',
			'transport' => 'postMessage'
		)
	);

	$wp_customize->add_control(
		new Kingcabs_Switch_Control(
			$wp_customize,
			'kingcabs_client_logo_section_disable',
			array(
				'settings'		=> 'kingcabs_client_logo_section_disable',
				'section'		=> 'kingcabs_client_logo_section',
				'label'			=> __( "Enable Section", "kingcabs" ),
				'switch_label' 	=> array(
					'on' => __( "Yes", "kingcabs" ),
					'off' => __( "No", "kingcabs" )
				)	
			)
		)
	);

	$wp_customize->add_setting(
		'kingcabs_client_logo_image',
		array(
			'sanitize_callback' => 'sanitize_text_field', // Done
			'transport' => 'postMessage'
		)
	);

	$wp_customize->add_control(
		new Kingcabs_Display_Gallery_Control(
			$wp_customize,
			'kingcabs_client_logo_image',
		array(
			'settings'		=> 'kingcabs_client_logo_image',
			'section'		=> 'kingcabs_client_logo_section',
			'label'			=> __( "Upload Client/Brand Logos", 'kingcabs' ),
		)
	));

	$wp_customize->selective_refresh->add_partial( 'kingcabs_client_logo', array(
		'settings' => array( 'kingcabs_client_logo_section_disable', 'kingcabs_client_logo_image' ),
		'selector' => '.kingcabs-clients',
		'container_inclusive' => true,
		'render_callback' => function () {
			return get_template_part( 'sections/section', 'clients' );
		}
	));

	$wp_customize->add_setting('kingcabs_client_logo_section_upgrade_text', array(
        'sanitize_callback' => 'kingcabs_sanitize_text'
    ));

    $wp_customize->add_control(new KingCabs_Upgrade_Text($wp_customize, 'kingcabs_client_logo_section_upgrade_text', array(
        'section' => 'kingcabs_client_logo_section',
        'label' => __("For more settings,", "kingcabs"),
        'choices' => array(
            __("Choose Between Slider Layout and List Layout", "kingcabs"),
        ),
        'priority' => 100
    )));

	$wp_customize->add_section(
		'kingcabs_social',
		array(
			'title' => __( "Social", "kingcabs" ),
			'panel' => 'kingcabs_home_panel'
		)
	);

	$wp_customize->add_setting(
		'kingcabs_footer_follow_facebook',
		array(
			'sanitize_callback' => 'esc_url_raw', // Done
			'default'			=> '',
			'transport'			=> 'postMessage'
		)
	);

	$wp_customize->add_control(
		'kingcabs_footer_follow_facebook',
		array(
			'settings'		=> 'kingcabs_footer_follow_facebook',
			'section'		=> 'kingcabs_social',
			'type'			=> 'url',
			'label'			=> __( "Facebook URL", "kingcabs" )
		)
	);

	$wp_customize->add_setting(
		'kingcabs_footer_follow_twitter',
		array(
			'sanitize_callback' => 'esc_url_raw', // Done
			'default'			=> '',
			'transport'			=> 'postMessage'
		)
	);

	$wp_customize->add_control(
		'kingcabs_footer_follow_twitter',
		array(
			'settings'		=> 'kingcabs_footer_follow_twitter',
			'section'		=> 'kingcabs_social',
			'type'			=> 'url',
			'label'			=> __( "Twitter URL", "kingcabs" )
		)
	);


	$wp_customize->add_setting(
		'kingcabs_footer_follow_youtube',
		array(
			'sanitize_callback' => 'esc_url_raw',  // Done
			'default'			=> '',
			'transport'			=> 'postMessage'
		)
	);

	$wp_customize->add_control(
		'kingcabs_footer_follow_youtube',
		array(
			'settings'		=> 'kingcabs_footer_follow_youtube',
			'section'		=> 'kingcabs_social',
			'type'			=> 'url',
			'label'			=> __( "YouTube URL", "kingcabs" )
		)
	);

	$wp_customize->add_setting(
		'kingcabs_footer_follow_ins',
		array(
			'sanitize_callback' => 'esc_url_raw',   // Done
			'default'			=> '',
			'transport'			=> 'postMessage'
		)
	);

	$wp_customize->add_control(
		'kingcabs_footer_follow_ins',
		array(
			'settings'		=> 'kingcabs_footer_follow_ins',
			'section'		=> 'kingcabs_social',
			'type'			=> 'url',
			'label'			=> __( "Instagram URL ", "kingcabs" )
		)
	);

	$wp_customize->add_setting(
		'kingcabs_footer_follow_linkedin',
		array(
			'sanitize_callback' => 'esc_url_raw',  // Done
			'default'			=> '',
			'transport'			=> 'postMessage'
		)
	);

	$wp_customize->add_control(
		'kingcabs_footer_follow_linkedin',
		array(
			'settings'		=> 'kingcabs_footer_follow_linkedin',
			'section'		=> 'kingcabs_social',
			'type'			=> 'url',
			'label'			=> __( "Linkendin URL", "kingcabs" )
		)
	);

	/*
	* Footer Secton Area
	*/
	$wp_customize->add_section(
		'kingcabs_footer_section',
		array(
			'title' => __( "Footer", "kingcabs" ),
			'panel' => 'kingcabs_home_panel'
		)
	);

	add_filter("kingcabs_footer_nav_content_tab", function() {
		return [
			'kingcabs_footer_top_title',
			'kingcabs_footer_top_button_title',
			'kingcabs_footer_top_button_url_title',
			'kingcabs_footer_top_button_url_title',
			'kingcabs_footer_buttom_copyright_setting',
			'kingcabs_link_to_social',
			'kingcabs_home_panel_upgrade_section',
		];
	});

	$wp_customize->add_setting(
		'kingcabs_footer_top_title',
		array(
			'sanitize_callback' => 'sanitize_text_field', // Done
			'default'			=> '',
			'transport' 		=> 'postMessage'
		)
	);

	$wp_customize->add_control(
		'kingcabs_footer_top_title',
		array(
			'settings'		=> 'kingcabs_footer_top_title',
			'section'		=> 'kingcabs_footer_section',
			'type'			=> 'text',
			'label'			=> __( "Title", "kingcabs" )
		)
	);

    $wp_customize->add_setting(
		'kingcabs_footer_top_button_title',
		array(
			'sanitize_callback' => 'sanitize_text_field', // Done
			'default'			=> '',
			'transport'			=> 'postMessage'
		)
	);

	$wp_customize->add_control(
		'kingcabs_footer_top_button_title',
		array(
			'settings'		=> 'kingcabs_footer_top_button_title',
			'section'		=> 'kingcabs_footer_section',
			'type'			=> 'text',
			'label'			=> __( "Button Text", "kingcabs" )
		)
	);

	$wp_customize->add_setting(
		'kingcabs_footer_top_button_url_title',
		array(
			'sanitize_callback' => 'sanitize_text_field',  // Done
			'default'			=> '',
			'transport'			=> 'postMessage'
		)
	);

	$wp_customize->add_control(
		'kingcabs_footer_top_button_url_title',
		array(
			'settings'		=> 'kingcabs_footer_top_button_url_title',
			'section'		=> 'kingcabs_footer_section',
			'type'			=> 'text',
			'label'			=> __( "Button Url", "kingcabs" )
		)
	);

	$wp_customize->selective_refresh->add_partial( 'kingcabs_social', array(
		'settings' => array( 'kingcabs_footer_follow_facebook', 'kingcabs_footer_follow_twitter', 'kingcabs_footer_follow_youtube', 'kingcabs_footer_follow_ins', 'kingcabs_footer_follow_linkedin' ),
		'selector' => '.kingcabs-social',
		'container_inclusive' => false,
		'render_callback' => function () {
			return do_action( 'kingcabs_social' );
		}
	));

	$wp_customize->add_setting('kingcabs_footer_buttom_copyright_setting', array(
		'default' => '',
		'sanitize_callback' => 'sanitize_text_field',  //done
		'transport' => 'postMessage'
	));

	$wp_customize->add_control('kingcabs_footer_buttom_copyright_setting', array(
		'type' => 'textarea',
		'label' => __("Copyright Text", "kingcabs"),
		'section' => 'kingcabs_footer_section',
		'settings' => 'kingcabs_footer_buttom_copyright_setting'
	) ); 

	$wp_customize->selective_refresh->add_partial( 'kingcabs_footer_buttom_copyright', array (
		'settings' => array( 'kingcabs_footer_buttom_copyright_setting' ),
		'selector' => '.footer-bottom .col-md-6:first-of-type .text-left',
		'container_inclusive' => false,
		'render_callback' => function () {
			return do_action( 'kingcabs_copyright', 5 );
		}
	));

	$wp_customize->add_setting('kingcabs_link_to_social', array(
		'sanitize_callback' => 'kingcabs_sanitize_text'
	));
	
	$wp_customize->add_control(new KingCabs_Link_To_Social($wp_customize, 'kingcabs_link_to_social', array(
		'section' => 'kingcabs_footer_section',
		'label' => __("Configure Social Settings", "kingcabs"),
		'priority' => 100,
	)));

	$wp_customize->add_section(new KingCabs_Customize_Upgrade_Section($wp_customize, 'kingcabs_home_panel_upgrade_section', array(
        'title' => __("More Sections on Premium", "kingcabs"),
        'panel' => 'kingcabs_home_panel',
        'priority' => 1000,
        'options' => array(
            __("- Video Call to Action", "kingcabs"),
            __("- Our Blog Section Settings", "kingcabs"),
            __("- Contact Us Settings", "kingcabs"),
            __("------------------------", "kingcabs"),
            __("Elementor Pagebuilder Compatible. All the above sections can be created with Elementor Page Builder or Customizer whichever you like.", "kingcabs")
        )
    )));


	$wp_customize->register_control_type('Kingcabs_Custom_Control_Tab');
	$wp_customize->register_control_type('Kingcabs_Background_Control');

	require_once get_template_directory() . '/sparklethemes/customizer/common-settings.php';


if ( isset( $wp_customize->selective_refresh ) ) {
	
	$wp_customize->selective_refresh->add_partial( 'blogname', array(
		'selector'        => '.site-title a',
		'render_callback' => 'kingcabs_customize_partial_blogname',
	) );

	$wp_customize->selective_refresh->add_partial( 'blogdescription', array(
		'selector'        => '.site-description',
		'render_callback' => 'kingcabs_customize_partial_blogdescription',
	) );

}

	//SANITIZATION FUNCTIONS
	function kingcabs_sanitize_text($input) {
	    return wp_kses_post(force_balance_tags($input));
	}

	function kingcabs_sanitize_choices_array( $input, $setting ) {
	    global $wp_customize;
	    
	    if(!empty($input)){
	        $input = array_map('absint', $input);
	    }

	    return $input;
	} 

	/**
     * Driver Column Sanitization
    */
    function kingcabs_driver_column($input) {
       $valid_keys = array(
          	'1' => '1',
          	'2' => '2',  
			'3' => '3', 
			'4' => '4',
       );
       if ( array_key_exists( $input, $valid_keys ) ) {
          return $input;
       } else {
          return '';
       }
    }

}
add_action( 'customize_register', 'kingcabs_customize_register' );

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function kingcabs_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function kingcabs_customize_partial_blogdescription() {
	bloginfo( 'description' );
}


/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function kingcabs_customize_preview_js() {
	wp_enqueue_script( 'kingcabs-customizer', get_template_directory_uri() . '/sparklethemes/customizer/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'kingcabs_customize_preview_js' );


/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function Kingcabs_customizer_script() {
    wp_enqueue_script( 'kingcabs-customizer-script', get_template_directory_uri() .'/sparklethemes/customizer/js/customizer-scripts.js', array("jquery"),'', true  );
    wp_enqueue_script( 'kingcabs-customizer-chosen-script', get_template_directory_uri() .'/sparklethemes/customizer/js/chosen.jquery.js', array("jquery"),'1.4.1', true  );
    wp_enqueue_style( 'kingcabs-customizer-chosen-style', get_template_directory_uri() .'/sparklethemes/customizer/css/chosen.css');
    wp_enqueue_style( 'kingcabs-customizer-style', get_template_directory_uri() .'/sparklethemes/customizer/css/customizer-style.css');
    wp_enqueue_style( 'font-awesome', get_template_directory_uri() .'/assets/library/font-awesome/css/font-awesome.css'); 
}
add_action( 'customize_controls_enqueue_scripts', 'kingcabs_customizer_script' );

