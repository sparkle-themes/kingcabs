<?php
$section_array = [
    'header',
    'image_carousel',
    'slider_button',
    'service',
    'services',
    'vidoe_call_to_action',
    'counter',
    'fleet',
    'testimonial',
    'driver',
    'call_to_action',
    'blog',
    'client_logo',
    'contactus',
    'footer'
];

$super_title_exclude_array = [ 'call_to_action', 'vidoe_call_to_action' ];
$exculde_section_array = $section_array;

foreach ($section_array as $id) {

    $wp_customize->add_setting("kingcabs_{$id}_section_nav", array(
        'transport' => 'postMessage',
        'sanitize_callback' => 'wp_kses_post',
    ));
    $wp_customize->add_control(new Kingcabs_Custom_Control_Tab($wp_customize, "kingcabs_{$id}_section_nav", array(
        'type' => 'tab',
        'section' => "kingcabs_{$id}_section",
        'priority' => 1,
        'buttons' => array(
            array(
                'name' => esc_html__('Content', 'kingcabs'),
                'fields' => apply_filters("kingcabs_{$id}_nav_content_tab", array(
                    "kingcabs_{$id}_page",
                    "kingcabs_{$id}_section_disable",
                    "kingcabs_{$id}_section_upgrade_text"
                )),
                'active' => true,
            ),
            array(
                'name' => esc_html__('Style', 'kingcabs'),
                'fields' => apply_filters("kingcabs_{$id}_nav_style_tab", array(
                    "kingcabs_{$id}_cs_heading",
                    "kingcabs_{$id}_super_title_color",
                    "kingcabs_{$id}_title_color",
                    "kingcabs_{$id}_text_color",
                    "kingcabs_{$id}_link_color",
                    "kingcabs_{$id}_link_hover_color",
                    "kingcabs_{$id}_section_style_upgrade_text",

                )),
            ),
            array(
                'name' => esc_html__('Advanced', 'kingcabs'),
                'fields' => apply_filters("kingcabs_{$id}_nav_advance_tab", array(
                    
                    "kingcabs_{$id}_bg_heading",
                    "kingcabs_{$id}_bg_type",
                    "kingcabs_{$id}_bg_color",
                    "kingcabs_{$id}_bg_gradient",
                    "kingcabs_{$id}_bg_image",
                    "kingcabs_{$id}_bg_video",
                    "kingcabs_{$id}_overlay_color",

                    "kingcabs_{$id}_mp_heading",
                    "kingcabs_{$id}_padding",
                    "kingcabs_{$id}_margin",
                    "kingcabs_{$id}_radius",
                   
                    "kingcabs_{$id}_cs_seperator",
					"kingcabs_{$id}_seperator0",
					"kingcabs_{$id}_section_seperator",
					"kingcabs_{$id}_seperator1",
					"kingcabs_{$id}_top_seperator",
					"kingcabs_{$id}_ts_color",
					"kingcabs_{$id}_ts_height",
					"kingcabs_{$id}_seperator2",
					"kingcabs_{$id}_bottom_seperator",
					"kingcabs_{$id}_bs_color",
					"kingcabs_{$id}_bs_height",

                    "kingcabs_{$id}_section_advance_upgrade_text",
                )),
            ),
        ),
    )));

    $wp_customize->add_setting("kingcabs_{$id}_bg_heading", array(
        'sanitize_callback' => 'sanitize_text_field'
    ));
    $wp_customize->add_control(new Kingcabs_Customize_Heading($wp_customize, "kingcabs_{$id}_bg_heading", array(
        'section' => "kingcabs_{$id}_section",
        'label' => esc_html__('Background', 'kingcabs'),
        'priority' => 15
    )));

    $wp_customize->add_setting("kingcabs_{$id}_bg_type", array(
        'default' => 'none',
        'sanitize_callback' => 'sanitize_text_field',
        'transport' => 'postMessage'
    ));
    $wp_customize->add_control("kingcabs_{$id}_bg_type", array(
        'section' => "kingcabs_{$id}_section",
        'type' => 'select',
        'label' => esc_html__('Background Type', 'kingcabs'),
        'choices' => array(
            'none' => esc_html__('Default', 'kingcabs'),
            'color-bg' => esc_html__('Color Background (Pro)', 'kingcabs'),
            'gradient-bg' => esc_html__('Gradient Background(Pro)', 'kingcabs'),
            'image-bg' => esc_html__('Image Background(Pro)', 'kingcabs'),
            'video-bg' => esc_html__('Video Background(Pro)', 'kingcabs')
        ),
        'priority' => 15
    ));
    
    $wp_customize->add_setting("kingcabs_{$id}_cs_heading", array(
        'sanitize_callback' => 'sanitize_text_field'
    ));
    $wp_customize->add_control(new Kingcabs_Customize_Heading($wp_customize, "kingcabs_{$id}_cs_heading", array(
        'section' => "kingcabs_{$id}_section",
        'label' => esc_html__('Color Settings', 'kingcabs'),
        
    )));

    

    $wp_customize->add_setting("kingcabs_{$id}_section_style_upgrade_text", array(
        'sanitize_callback' => 'kingcabs_sanitize_text'
    ));

    $wp_customize->add_control(new KingCabs_Upgrade_Text($wp_customize, "kingcabs_{$id}_section_style_upgrade_text", array(
        'section' => "kingcabs_{$id}_section",
        'label' => __('For more styles and settings,', 'kingcabs'),
        'choices' => array(
            __('Multiple Layout Options', 'kingcabs'),
            __('Title Color', 'kingcabs'),
            __('Subtitle Color', 'kingcabs'),
            __('Content Color', 'kingcabs'),
            __('Link Color', 'kingcabs'),
            __('Hover Color', 'kingcabs'),
            __('And many more...', 'kingcabs'),
        ),
        'priority' => 100
    )));


     $wp_customize->add_setting("kingcabs_{$id}_section_advance_upgrade_text", array(
        'sanitize_callback' => 'kingcabs_sanitize_text'
    ));

    $wp_customize->add_control(new KingCabs_Upgrade_Text($wp_customize, "kingcabs_{$id}_section_advance_upgrade_text", array(
        'section' => "kingcabs_{$id}_section",
        'label' => __('For more styles and settings,', 'kingcabs'),
        'choices' => array(
            __('Background Options', 'kingcabs'),
            __('-Background Color', 'kingcabs'),
            __('-Background Image', 'kingcabs'),
            __('-Background Video', 'kingcabs'),
            __('-Background Gradient', 'kingcabs'),
            __('Section Margin / Padding Option', 'kingcabs'),
            __('Section Radius Option', 'kingcabs'),
            __('Section Seprator', 'kingcabs'),
            __(' - Top Seprator', 'kingcabs'),
            __(' - Bottom Seprator', 'kingcabs'),
            __('And many more...', 'kingcabs'),
        ),
        'priority' => 100
    )));

    

    
    
    $wp_customize->add_setting("kingcabs_{$id}_mp_heading", array(
        'sanitize_callback' => 'sanitize_text_field'
    ));
    $wp_customize->add_control(new Kingcabs_Customize_Heading($wp_customize, "kingcabs_{$id}_mp_heading", array(
        'section' => "kingcabs_{$id}_section",
        'label' => esc_html__('Margin/Padding', 'kingcabs'),
        'priority' => 80
    )));

    /** padding */
    $wp_customize->add_setting(
        "kingcabs_{$id}_padding",
        array(
            'transport' => 'postMessage',
            'sanitize_callback' => 'kingcabs_sanitize_field_default_css_box'
        )
    );
    $wp_customize->add_control(
        new Kingcabs_Custom_Control_Cssbox(
            $wp_customize,
            "kingcabs_{$id}_padding",
            array(
                'label'    => esc_html__( 'Padding (px)(PRO)', 'kingcabs' ),
                'section' => "kingcabs_{$id}_section",
                'settings' => "kingcabs_{$id}_padding",
                'priority' => 80
            ),
            array(),
            array()
        )
    );

    $wp_customize->add_setting(
        "kingcabs_{$id}_margin",
        array(
            'transport' => 'postMessage',
            'sanitize_callback' => 'kingcabs_sanitize_field_default_css_box'
        )
    );
    $wp_customize->add_control(
        new Kingcabs_Custom_Control_Cssbox(
            $wp_customize,
            "kingcabs_{$id}_margin",
            array(
                'label'    => esc_html__( 'Margin (px)(PRO)', 'kingcabs' ),
                'section' => "kingcabs_{$id}_section",
                'settings' => "kingcabs_{$id}_margin",
                'priority' => 80
            ),
            array(),
            array()
        )
    );

    $wp_customize->add_setting(
        "kingcabs_{$id}_radius",
        array(
            'transport' => 'postMessage',
            'sanitize_callback' => 'kingcabs_sanitize_field_default_css_box'
        )
    );
    $wp_customize->add_control(
        new Kingcabs_Custom_Control_Cssbox(
            $wp_customize,
            "kingcabs_{$id}_radius",
            array(
                'label'    => esc_html__( 'Radius (px)(PRO)', 'kingcabs' ),
                'section' => "kingcabs_{$id}_section",
                'settings' => "kingcabs_{$id}_radius",
                'priority' => 80
            ),
            array(),
            array()
        )
    );


    $wp_customize->add_setting("kingcabs_{$id}_section_seperator", array(
        'sanitize_callback' => 'sanitize_text_field',
        'default' => 'no',
        'transport' => 'postMessage'
    ));
    $wp_customize->add_control("kingcabs_{$id}_section_seperator", array(
        'section' => "kingcabs_{$id}_section",
        'type' => 'select',
        'label' => esc_html__('Enable Separator', 'kingcabs'),
        'choices' => array(
            'no' => esc_html__('Disable', 'kingcabs'),
            'top' => esc_html__('Enable Top Separator(Pro)', 'kingcabs'),
            'bottom' => esc_html__('Enable Bottom Separator(Pro)', 'kingcabs'),
            'top-bottom' => esc_html__('Enable Top & Bottom Separator(Pro)', 'kingcabs')
        ),
        'priority' => 95
    ));

}