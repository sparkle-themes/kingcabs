<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package King_Cabs
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> <?php kingcabs_html_tag_schema(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<header class="main-header <?php echo esc_attr(get_theme_mod('kingcabs_header_layout', 'headerone') ); ?>" <?php if( get_header_image() ){ ?> style="background-image:url(<?php header_image(); ?>)"<?php } ?>>

    <div class="top-header">
        <div class="container">
            <div class="row">

                <div class="col-md-7 col-sm-7 col-xs-7">
                    <div class="topheader-list">
                        <?php
    						wp_nav_menu( array(
    							'theme_location' => 'menu-2',
    							'menu_id'        => 'topmenu',
    							'depth'          => 2,				
    	                    ));
    					?>
                    </div>
                </div>

                <div class="col-md-5 col-sm-5 col-xs-5 text-right">
                    <div class="topheader-list">
                        <ul class="kingcabs-social">
                            <?php do_action( 'kingcabs_social' ); ?>
    					</ul>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- top header end -->

     <div class="logo-nav-wrapper">
        <div class="logo-section">
            <div class="container clearfix">
                <div class="logo-left">
                    <div class="logo site-branding">
                        <?php
                            if ( function_exists( 'the_custom_logo' ) ) {
                                the_custom_logo();
                            }
                        ?>
                        <h1 class="site-title">
                            <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
                                <?php bloginfo( 'name' ); ?>
                            </a>
                        </h1>
                        <?php
                            $description = get_bloginfo( 'description', 'display' );
                            if ( $description || is_customize_preview() ) : 
                        ?>
                            <p class="site-description"><?php echo $description; ?></p>
                        <?php endif; ?>
                    </div><!-- .site-branding & End Logo -->
                </div>

                <?php do_action('kingcab_get_header_info'); ?>
            </div>
        </div> <!--Header-Lower-->

        <div class="header-lower">
            <div class="container">
                <nav id="site-navigation" class="main-navigation clearfix">
                    <button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">
                        <i class="fa fa-bars"></i>
                    </button>
                    <div class="clearfix"></div>
                    <?php
                        wp_nav_menu( array(
                            'theme_location' => 'menu-1',
                            'menu_id'        => 'primary-menu',
                            'menu_class'     => 'nav-menu',
                            'fallback_cb'    => 'wp_page_menu',
                        ) );
                    ?>
                </nav><!-- #site-navigation -->
            </div>
        </div>
    </div>

</header><!--header end-->

<?php
  /**
   * Hook - kingcabs_head.
   *
   * @hooked kingcabs_breadcumbs - 10
   */
  do_action( 'kingcabs_breadcumbs', 10 );
?> 

<div class="clearfix"></div>