<?php
 /**
  * Title: Counter
  * Slug: kingcabs/counter
  * Categories: kingcabs
  */
?>
<!-- wp:group {"style":{"border":{"width":"0px","style":"none"},"spacing":{"padding":{"top":"50px","bottom":"80px"}}},"backgroundColor":"black","layout":{"inherit":true,"type":"constrained"}} -->
<div class="wp-block-group has-black-background-color has-background" style="border-style:none;border-width:0px;padding-top:50px;padding-bottom:80px"><!-- wp:group {"align":"wide","style":{"spacing":{"blockGap":"10px","padding":{"bottom":"30px"}}},"layout":{"inherit":false}} -->
<div class="wp-block-group alignwide" style="padding-bottom:30px"><!-- wp:group {"style":{"spacing":{"blockGap":"15px"}},"layout":{"inherit":true,"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:heading {"textAlign":"center","style":{"typography":{"lineHeight":"1.3","fontSize":"30px","fontStyle":"normal","fontWeight":"600"}},"textColor":"background","className":" animated animated-fadeInUp"} -->
<h2 class="wp-block-heading has-text-align-center animated animated-fadeInUp has-background-color has-text-color" style="font-size:30px;font-style:normal;font-weight:600;line-height:1.3">KINGCABS COUNTERS</h2>
<!-- /wp:heading -->

<!-- wp:group {"layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"center"}} -->
<div class="wp-block-group"><!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:separator {"backgroundColor":"background"} -->
<hr class="wp-block-separator has-text-color has-background-color has-alpha-channel-opacity has-background-background-color has-background"/>
<!-- /wp:separator --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"color":{"text":"#d9c77e"}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group has-text-color" style="color:#d9c77e"></div>
<!-- /wp:group -->

<!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:separator {"backgroundColor":"background"} -->
<hr class="wp-block-separator has-text-color has-background-color has-alpha-channel-opacity has-background-background-color has-background"/>
<!-- /wp:separator --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:paragraph {"align":"center","style":{"typography":{"fontStyle":"normal","fontWeight":"300"}},"textColor":"background","fontSize":"medium"} -->
<p class="has-text-align-center has-background-color has-text-color has-medium-font-size" style="font-style:normal;font-weight:300">Lorem ipsum dolor sit amet, consectetuer adipiscing elit enean commodo ligula..</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:columns -->
<div class="wp-block-columns"><!-- wp:column {"verticalAlignment":"center"} -->
<div class="wp-block-column is-vertically-aligned-center"><!-- wp:group {"align":"wide","style":{"spacing":{"blockGap":"10px","padding":{"top":"0px","right":"0px","bottom":"20px","left":"0px"}},"border":{"radius":{"topLeft":"5px","topRight":"5px"}},"color":{"background":"#ffffff00"}},"className":"animated animated-fadeInUp","layout":{"inherit":true,"type":"constrained"}} -->
<div class="wp-block-group alignwide animated animated-fadeInUp has-background" style="border-top-left-radius:5px;border-top-right-radius:5px;background-color:#ffffff00;padding-top:0px;padding-right:0px;padding-bottom:20px;padding-left:0px"><!-- wp:group {"layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group"><!-- wp:group {"layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group"><!-- wp:group {"style":{"color":{"text":"#d9c77e"}}} -->
<div class="wp-block-group has-text-color" style="color:#d9c77e"></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:group {"layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group"><!-- wp:group {"style":{"spacing":{"blockGap":"0"}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:heading {"style":{"typography":{"fontStyle":"normal","fontWeight":"600","fontSize":"70px"}},"textColor":"background"} -->
<h2 class="wp-block-heading has-background-color has-text-color" style="font-size:70px;font-style:normal;font-weight:600">20</h2>
<!-- /wp:heading -->

<!-- wp:heading {"textAlign":"center","level":3,"style":{"typography":{"fontStyle":"normal","fontWeight":"500"}},"textColor":"background","fontSize":"extra-small"} -->
<h3 class="wp-block-heading has-text-align-center has-background-color has-text-color has-extra-small-font-size" style="font-style:normal;font-weight:500">CARS</h3>
<!-- /wp:heading --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column {"verticalAlignment":"center"} -->
<div class="wp-block-column is-vertically-aligned-center"><!-- wp:group {"align":"wide","style":{"spacing":{"blockGap":"10px","padding":{"top":"0px","right":"0px","bottom":"20px","left":"0px"}},"border":{"radius":{"topLeft":"5px","topRight":"5px"}},"color":{"background":"#ffffff00"}},"className":"animated animated-fadeInUp","layout":{"inherit":true,"type":"constrained"}} -->
<div class="wp-block-group alignwide animated animated-fadeInUp has-background" style="border-top-left-radius:5px;border-top-right-radius:5px;background-color:#ffffff00;padding-top:0px;padding-right:0px;padding-bottom:20px;padding-left:0px"><!-- wp:group {"layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group"><!-- wp:group {"layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group"><!-- wp:group {"style":{"color":{"text":"#d9c77e"}}} -->
<div class="wp-block-group has-text-color" style="color:#d9c77e"></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:group {"layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group"><!-- wp:group {"style":{"spacing":{"blockGap":"0"}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:heading {"style":{"typography":{"fontStyle":"normal","fontWeight":"600","fontSize":"70px"}},"textColor":"background"} -->
<h2 class="wp-block-heading has-background-color has-text-color" style="font-size:70px;font-style:normal;font-weight:600">500</h2>
<!-- /wp:heading -->

<!-- wp:heading {"textAlign":"center","level":3,"style":{"typography":{"fontStyle":"normal","fontWeight":"500"}},"textColor":"background","fontSize":"extra-small"} -->
<h3 class="wp-block-heading has-text-align-center has-background-color has-text-color has-extra-small-font-size" style="font-style:normal;font-weight:500">TIPS</h3>
<!-- /wp:heading --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column {"verticalAlignment":"center"} -->
<div class="wp-block-column is-vertically-aligned-center"><!-- wp:group {"align":"wide","style":{"spacing":{"blockGap":"10px","padding":{"top":"0px","right":"0px","bottom":"20px","left":"0px"}},"border":{"radius":{"topLeft":"5px","topRight":"5px"}},"color":{"background":"#ffffff00"}},"className":"animated animated-fadeInUp","layout":{"inherit":true,"type":"constrained"}} -->
<div class="wp-block-group alignwide animated animated-fadeInUp has-background" style="border-top-left-radius:5px;border-top-right-radius:5px;background-color:#ffffff00;padding-top:0px;padding-right:0px;padding-bottom:20px;padding-left:0px"><!-- wp:group {"layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group"><!-- wp:group {"layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group"><!-- wp:group {"style":{"color":{"text":"#d9c77e"}}} -->
<div class="wp-block-group has-text-color" style="color:#d9c77e"></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:group {"layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group"><!-- wp:group {"style":{"spacing":{"blockGap":"0"}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:heading {"style":{"typography":{"fontStyle":"normal","fontWeight":"600","fontSize":"70px"}},"textColor":"background"} -->
<h2 class="wp-block-heading has-background-color has-text-color" style="font-size:70px;font-style:normal;font-weight:600">200</h2>
<!-- /wp:heading -->

<!-- wp:heading {"textAlign":"center","level":3,"style":{"typography":{"fontStyle":"normal","fontWeight":"500"}},"textColor":"background","fontSize":"extra-small"} -->
<h3 class="wp-block-heading has-text-align-center has-background-color has-text-color has-extra-small-font-size" style="font-style:normal;font-weight:500">CLIENT</h3>
<!-- /wp:heading --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column {"verticalAlignment":"center"} -->
<div class="wp-block-column is-vertically-aligned-center"><!-- wp:group {"align":"wide","style":{"spacing":{"blockGap":"10px","padding":{"top":"0px","right":"0px","bottom":"20px","left":"0px"}},"border":{"radius":{"topLeft":"5px","topRight":"5px"}},"color":{"background":"#ffffff00"}},"className":"animated animated-fadeInUp","layout":{"inherit":true,"type":"constrained"}} -->
<div class="wp-block-group alignwide animated animated-fadeInUp has-background" style="border-top-left-radius:5px;border-top-right-radius:5px;background-color:#ffffff00;padding-top:0px;padding-right:0px;padding-bottom:20px;padding-left:0px"><!-- wp:group {"layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group"><!-- wp:group {"layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group"><!-- wp:group {"style":{"color":{"text":"#d9c77e"}}} -->
<div class="wp-block-group has-text-color" style="color:#d9c77e"></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:group {"layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group"><!-- wp:group {"style":{"spacing":{"blockGap":"0"}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:heading {"style":{"typography":{"fontStyle":"normal","fontWeight":"600","fontSize":"60px"}},"textColor":"background"} -->
<h2 class="wp-block-heading has-background-color has-text-color" style="font-size:60px;font-style:normal;font-weight:600">1000</h2>
<!-- /wp:heading -->

<!-- wp:heading {"textAlign":"center","level":3,"style":{"typography":{"fontStyle":"normal","fontWeight":"500","textTransform":"uppercase"}},"textColor":"background","fontSize":"extra-small"} -->
<h3 class="wp-block-heading has-text-align-center has-background-color has-text-color has-extra-small-font-size" style="font-style:normal;font-weight:500;text-transform:uppercase">Kilometer</h3>
<!-- /wp:heading --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:column --></div>
<!-- /wp:columns --></div>
<!-- /wp:group -->