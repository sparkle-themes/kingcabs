<?php
 /**
  * Title: Testimonial
  * Slug: kingcabs/testimonial
  * Categories: kingcabs
  */
?>
<!-- wp:group {"style":{"spacing":{"padding":{"top":"100px","right":"20px","bottom":"100px","left":"20px"}}},"backgroundColor":"black","layout":{"type":"constrained"}} -->
<div class="wp-block-group has-black-background-color has-background" style="padding-top:100px;padding-right:20px;padding-bottom:100px;padding-left:20px"><!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:heading {"textAlign":"center","style":{"typography":{"fontSize":"30px","fontStyle":"normal","fontWeight":"500"}},"textColor":"background"} -->
<h2 class="wp-block-heading has-text-align-center has-background-color has-text-color" style="font-size:30px;font-style:normal;font-weight:500">WHAT OUR CUSTOMER SAY</h2>
<!-- /wp:heading -->

<!-- wp:group {"layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"center"}} -->
<div class="wp-block-group"><!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:separator {"backgroundColor":"background"} -->
<hr class="wp-block-separator has-text-color has-background-color has-alpha-channel-opacity has-background-background-color has-background"/>
<!-- /wp:separator --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"color":{"text":"#d7c77e"}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group has-text-color" style="color:#d7c77e"></div>
<!-- /wp:group -->

<!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:separator {"backgroundColor":"background-secondary"} -->
<hr class="wp-block-separator has-text-color has-background-secondary-color has-alpha-channel-opacity has-background-secondary-background-color has-background"/>
<!-- /wp:separator --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:paragraph {"align":"center","style":{"typography":{"fontStyle":"normal","fontWeight":"300"}},"textColor":"background","fontSize":"medium"} -->
<p class="has-text-align-center has-background-color has-text-color has-medium-font-size" style="font-style:normal;font-weight:300">Lorem ipsum dolor sit amet, consectetuer adipiscing elit enean commodo ligula.</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group -->

<!-- wp:columns {"verticalAlignment":"center","align":"wide","style":{"spacing":{"padding":{"top":"40px"}}}} -->
<div class="wp-block-columns alignwide are-vertically-aligned-center" style="padding-top:40px"><!-- wp:column {"verticalAlignment":"center"} -->
<div class="wp-block-column is-vertically-aligned-center"><!-- wp:group {"align":"wide","style":{"spacing":{"blockGap":"10px","padding":{"top":"0px","right":"0px","bottom":"20px","left":"0px"}},"border":{"radius":"5px"}},"className":"  animated animated-fadeInUp","layout":{"inherit":true,"type":"constrained"}} -->
<div class="wp-block-group alignwide animated animated-fadeInUp" style="border-radius:5px;padding-top:0px;padding-right:0px;padding-bottom:20px;padding-left:0px"><!-- wp:group -->
<div class="wp-block-group"><!-- wp:group {"align":"full","style":{"color":{"text":"#d9c77e"}},"layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"center"}} -->
<div class="wp-block-group alignfull has-text-color" style="color:#d9c77e"><!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:image {"id":188,"width":85,"height":85,"sizeSlug":"full","linkDestination":"none","className":"is-style-rounded"} -->
<figure class="wp-block-image size-full is-resized is-style-rounded"><img src="https://demo.sparkletheme.com/sparkle-fse/limousine/wp-content/uploads/sites/6/2022/12/client2.jpg" alt="" class="wp-image-188" width="85" height="85"/></figure>
<!-- /wp:image --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"spacing":{"blockGap":"10px"}},"textColor":"background","layout":{"type":"constrained"}} -->
<div class="wp-block-group has-background-color has-text-color"><!-- wp:paragraph {"align":"center","style":{"typography":{"fontStyle":"normal","fontWeight":"300"}},"textColor":"background","className":"sp-p-lr","fontSize":"medium"} -->
<p class="has-text-align-center sp-p-lr has-background-color has-text-color has-medium-font-size" style="font-style:normal;font-weight:300">Lorem Ipsum&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s,...</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"textAlign":"center","style":{"typography":{"fontSize":"16px","fontStyle":"normal","fontWeight":"600"},"color":{"text":"#d7c77e"}}} -->
<h2 class="wp-block-heading has-text-align-center has-text-color" style="color:#d7c77e;font-size:16px;font-style:normal;font-weight:600">John Doe</h2>
<!-- /wp:heading --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column {"verticalAlignment":"center"} -->
<div class="wp-block-column is-vertically-aligned-center"><!-- wp:group {"align":"wide","style":{"spacing":{"blockGap":"10px","padding":{"top":"0px","right":"0px","bottom":"20px","left":"0px"}},"border":{"radius":"5px"}},"className":"  animated animated-fadeInUp","layout":{"inherit":true,"type":"constrained"}} -->
<div class="wp-block-group alignwide animated animated-fadeInUp" style="border-radius:5px;padding-top:0px;padding-right:0px;padding-bottom:20px;padding-left:0px"><!-- wp:group -->
<div class="wp-block-group"><!-- wp:group {"align":"full","style":{"color":{"text":"#d9c77e"}},"layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"center"}} -->
<div class="wp-block-group alignfull has-text-color" style="color:#d9c77e"><!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:image {"id":197,"sizeSlug":"full","linkDestination":"none","className":"is-style-rounded"} -->
<figure class="wp-block-image size-full is-style-rounded"><img src="https://demo.sparkletheme.com/sparkle-fse/limousine/wp-content/uploads/sites/6/2022/12/1-5.jpg" alt="" class="wp-image-197"/></figure>
<!-- /wp:image --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"spacing":{"blockGap":"10px"}},"textColor":"background","layout":{"type":"constrained"}} -->
<div class="wp-block-group has-background-color has-text-color"><!-- wp:paragraph {"align":"center","style":{"typography":{"fontStyle":"normal","fontWeight":"300"}},"textColor":"background","className":"sp-p-lr","fontSize":"medium"} -->
<p class="has-text-align-center sp-p-lr has-background-color has-text-color has-medium-font-size" style="font-style:normal;font-weight:300">Lorem Ipsum&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s,...</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"textAlign":"center","style":{"typography":{"fontSize":"16px","fontStyle":"normal","fontWeight":"600"},"color":{"text":"#d7c77e"}}} -->
<h2 class="wp-block-heading has-text-align-center has-text-color" style="color:#d7c77e;font-size:16px;font-style:normal;font-weight:600">John Doe</h2>
<!-- /wp:heading --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column {"verticalAlignment":"center"} -->
<div class="wp-block-column is-vertically-aligned-center"><!-- wp:group {"align":"wide","style":{"spacing":{"blockGap":"10px","padding":{"top":"0px","right":"0px","bottom":"20px","left":"0px"}},"border":{"radius":"5px"}},"className":"  animated animated-fadeInUp","layout":{"inherit":true,"type":"constrained"}} -->
<div class="wp-block-group alignwide animated animated-fadeInUp" style="border-radius:5px;padding-top:0px;padding-right:0px;padding-bottom:20px;padding-left:0px"><!-- wp:group -->
<div class="wp-block-group"><!-- wp:group {"align":"full","style":{"color":{"text":"#d9c77e"}},"layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"center"}} -->
<div class="wp-block-group alignfull has-text-color" style="color:#d9c77e"><!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:image {"id":198,"sizeSlug":"full","linkDestination":"none","className":"is-style-rounded"} -->
<figure class="wp-block-image size-full is-style-rounded"><img src="https://demo.sparkletheme.com/sparkle-fse/limousine/wp-content/uploads/sites/6/2022/12/2-3.jpg" alt="" class="wp-image-198"/></figure>
<!-- /wp:image --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"spacing":{"blockGap":"10px"}},"textColor":"background","layout":{"type":"constrained"}} -->
<div class="wp-block-group has-background-color has-text-color"><!-- wp:paragraph {"align":"center","style":{"typography":{"fontStyle":"normal","fontWeight":"300"}},"textColor":"background","className":"sp-p-lr","fontSize":"medium"} -->
<p class="has-text-align-center sp-p-lr has-background-color has-text-color has-medium-font-size" style="font-style:normal;font-weight:300">Lorem Ipsum&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s,...</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"textAlign":"center","style":{"typography":{"fontSize":"16px","fontStyle":"normal","fontWeight":"600"},"color":{"text":"#d7c77e"}}} -->
<h2 class="wp-block-heading has-text-align-center has-text-color" style="color:#d7c77e;font-size:16px;font-style:normal;font-weight:600">John Doe</h2>
<!-- /wp:heading --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:column --></div>
<!-- /wp:columns --></div>
<!-- /wp:group -->