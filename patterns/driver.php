<?php
 /**
  * Title: Driver
  * Slug: kingcabs/driver
  * Categories: kingcabs
  */
?>
<!-- wp:group {"align":"wide","style":{"border":{"width":"0px","style":"none"},"color":{"background":"#fefefe"},"spacing":{"padding":{"top":"50px","bottom":"80px"}}},"layout":{"inherit":true,"type":"constrained"}} -->
<div class="wp-block-group alignwide has-background" style="border-style:none;border-width:0px;background-color:#fefefe;padding-top:50px;padding-bottom:80px"><!-- wp:group {"align":"wide","style":{"spacing":{"blockGap":"10px","padding":{"bottom":"30px"}}},"layout":{"inherit":false}} -->
<div class="wp-block-group alignwide" style="padding-bottom:30px"><!-- wp:group {"style":{"spacing":{"blockGap":"15px"}},"layout":{"inherit":true,"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:heading {"textAlign":"center","style":{"typography":{"lineHeight":"1.3","fontSize":"30px","fontStyle":"normal","fontWeight":"500"}},"className":" animated animated-fadeInUp"} -->
<h2 class="wp-block-heading has-text-align-center animated animated-fadeInUp" style="font-size:30px;font-style:normal;font-weight:500;line-height:1.3">OUR DRIVERS</h2>
<!-- /wp:heading -->

<!-- wp:group {"layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"center"}} -->
<div class="wp-block-group"><!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:separator {"backgroundColor":"foreground"} -->
<hr class="wp-block-separator has-text-color has-foreground-color has-alpha-channel-opacity has-foreground-background-color has-background"/>
<!-- /wp:separator --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"color":{"text":"#d7c77e"}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group has-text-color" style="color:#d7c77e"></div>
<!-- /wp:group -->

<!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:separator {"backgroundColor":"foreground"} -->
<hr class="wp-block-separator has-text-color has-foreground-color has-alpha-channel-opacity has-foreground-background-color has-background"/>
<!-- /wp:separator --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:paragraph {"align":"center","style":{"typography":{"fontStyle":"normal","fontWeight":"300"}},"textColor":"foreground","fontSize":"medium"} -->
<p class="has-text-align-center has-foreground-color has-text-color has-medium-font-size" style="font-style:normal;font-weight:300">Lorem ipsum dolor sit amet, consectetuer adipiscing elit enean commodo ligula.</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group -->

<!-- wp:columns {"align":"wide"} -->
<div class="wp-block-columns alignwide"><!-- wp:column {"verticalAlignment":"center","style":{"spacing":{"blockGap":"0"}}} -->
<div class="wp-block-column is-vertically-aligned-center"><!-- wp:group {"align":"wide","style":{"spacing":{"blockGap":"10px","padding":{"top":"0px","right":"0px","bottom":"20px","left":"0px"}},"border":{"radius":{"topLeft":"5px","topRight":"5px"}},"color":{"background":"#ffffff00"}},"className":"animated animated-fadeInUp","layout":{"inherit":true,"type":"constrained"}} -->
<div class="wp-block-group alignwide animated animated-fadeInUp has-background" style="border-top-left-radius:5px;border-top-right-radius:5px;background-color:#ffffff00;padding-top:0px;padding-right:0px;padding-bottom:20px;padding-left:0px"><!-- wp:group -->
<div class="wp-block-group"><!-- wp:image {"align":"center","id":209,"width":367,"height":367,"sizeSlug":"full","linkDestination":"none","style":{"border":{"radius":{"topLeft":"5px","topRight":"5px","bottomLeft":"0px","bottomRight":"0px"}}}} -->
<figure class="wp-block-image aligncenter size-full is-resized has-custom-border"><img src="https://demo.sparkletheme.com/sparkle-fse/limousine/wp-content/uploads/sites/6/2022/12/image-2-3.png" alt="" class="wp-image-209" style="border-top-left-radius:5px;border-top-right-radius:5px;border-bottom-left-radius:0px;border-bottom-right-radius:0px" width="367" height="367"/></figure>
<!-- /wp:image --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"spacing":{"blockGap":"2px","margin":{"top":"0","bottom":"0"},"padding":{"top":"10px","right":"0px","bottom":"10px","left":"0px"}}},"backgroundColor":"foreground","className":" has-shadow-dark has-width animated animated-fadeInUp"} -->
<div class="wp-block-group has-shadow-dark has-width animated animated-fadeInUp has-foreground-background-color has-background" style="margin-top:0;margin-bottom:0;padding-top:10px;padding-right:0px;padding-bottom:10px;padding-left:0px"><!-- wp:heading {"textAlign":"center","level":3,"style":{"typography":{"fontStyle":"normal","fontWeight":"300"}},"textColor":"background","fontSize":"medium"} -->
<h3 class="wp-block-heading has-text-align-center has-background-color has-text-color has-medium-font-size" style="font-style:normal;font-weight:300">Denish Richh</h3>
<!-- /wp:heading -->

<!-- wp:paragraph {"align":"center","style":{"color":{"text":"#a28023"}},"className":"sp-p-lr","fontSize":"extra-small"} -->
<p class="has-text-align-center sp-p-lr has-text-color has-extra-small-font-size" style="color:#a28023">BMW | Drvier</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column {"verticalAlignment":"center"} -->
<div class="wp-block-column is-vertically-aligned-center"><!-- wp:group {"align":"wide","style":{"spacing":{"blockGap":"10px","padding":{"top":"0px","right":"0px","bottom":"20px","left":"0px"}},"border":{"radius":{"topLeft":"5px","topRight":"5px"}},"color":{"background":"#ffffff00"}},"className":"animated animated-fadeInUp","layout":{"inherit":true,"type":"constrained"}} -->
<div class="wp-block-group alignwide animated animated-fadeInUp has-background" style="border-top-left-radius:5px;border-top-right-radius:5px;background-color:#ffffff00;padding-top:0px;padding-right:0px;padding-bottom:20px;padding-left:0px"><!-- wp:group -->
<div class="wp-block-group"><!-- wp:image {"align":"center","id":203,"width":367,"height":367,"sizeSlug":"full","linkDestination":"none","style":{"border":{"radius":{"topLeft":"5px","topRight":"5px","bottomLeft":"0px","bottomRight":"0px"}}}} -->
<figure class="wp-block-image aligncenter size-full is-resized has-custom-border"><img src="https://demo.sparkletheme.com/sparkle-fse/limousine/wp-content/uploads/sites/6/2022/12/image-2-2.png" alt="" class="wp-image-203" style="border-top-left-radius:5px;border-top-right-radius:5px;border-bottom-left-radius:0px;border-bottom-right-radius:0px" width="367" height="367"/></figure>
<!-- /wp:image --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"spacing":{"padding":{"top":"10px","right":"10px","bottom":"10px","left":"10px"},"blockGap":"2px","margin":{"top":"0","bottom":"0"}}},"backgroundColor":"foreground","className":" has-shadow-dark has-width   animated animated-fadeInUp"} -->
<div class="wp-block-group has-shadow-dark has-width animated animated-fadeInUp has-foreground-background-color has-background" style="margin-top:0;margin-bottom:0;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px"><!-- wp:heading {"textAlign":"center","level":3,"style":{"typography":{"fontStyle":"normal","fontWeight":"300"}},"textColor":"background","fontSize":"normal"} -->
<h3 class="wp-block-heading has-text-align-center has-background-color has-text-color has-normal-font-size" style="font-style:normal;font-weight:300">Jasmine Obama</h3>
<!-- /wp:heading -->

<!-- wp:paragraph {"align":"center","style":{"color":{"text":"#a28023"}},"className":"sp-p-lr","fontSize":"extra-small"} -->
<p class="has-text-align-center sp-p-lr has-text-color has-extra-small-font-size" style="color:#a28023">BMW | Driver</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column {"verticalAlignment":"center"} -->
<div class="wp-block-column is-vertically-aligned-center"><!-- wp:group {"align":"wide","style":{"spacing":{"blockGap":"10px","padding":{"top":"0px","right":"0px","bottom":"20px","left":"0px"}},"border":{"radius":{"topLeft":"5px","topRight":"5px"}},"color":{"background":"#ffffff00"}},"className":"animated animated-fadeInUp","layout":{"inherit":true,"type":"constrained"}} -->
<div class="wp-block-group alignwide animated animated-fadeInUp has-background" style="border-top-left-radius:5px;border-top-right-radius:5px;background-color:#ffffff00;padding-top:0px;padding-right:0px;padding-bottom:20px;padding-left:0px"><!-- wp:group -->
<div class="wp-block-group"><!-- wp:image {"align":"center","id":202,"width":367,"height":367,"sizeSlug":"full","linkDestination":"none","style":{"border":{"radius":{"topLeft":"5px","topRight":"5px","bottomLeft":"0px","bottomRight":"0px"}}}} -->
<figure class="wp-block-image aligncenter size-full is-resized has-custom-border"><img src="https://demo.sparkletheme.com/sparkle-fse/limousine/wp-content/uploads/sites/6/2022/12/image-2-1.png" alt="" class="wp-image-202" style="border-top-left-radius:5px;border-top-right-radius:5px;border-bottom-left-radius:0px;border-bottom-right-radius:0px" width="367" height="367"/></figure>
<!-- /wp:image --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"spacing":{"padding":{"top":"10px","right":"10px","bottom":"10px","left":"10px"},"blockGap":"2px","margin":{"top":"0","bottom":"0"}}},"backgroundColor":"foreground","className":"has-shadow-dark has-width   animated animated-fadeInUp"} -->
<div class="wp-block-group has-shadow-dark has-width animated animated-fadeInUp has-foreground-background-color has-background" style="margin-top:0;margin-bottom:0;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px"><!-- wp:heading {"textAlign":"center","level":3,"style":{"typography":{"fontStyle":"normal","fontWeight":"300"}},"textColor":"background","fontSize":"normal"} -->
<h3 class="wp-block-heading has-text-align-center has-background-color has-text-color has-normal-font-size" style="font-style:normal;font-weight:300">Jason Matt</h3>
<!-- /wp:heading -->

<!-- wp:paragraph {"align":"center","style":{"color":{"text":"#a28023"}},"className":"sp-p-lr","fontSize":"extra-small"} -->
<p class="has-text-align-center sp-p-lr has-text-color has-extra-small-font-size" style="color:#a28023">BMW | Driver</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:column --></div>
<!-- /wp:columns --></div>
<!-- /wp:group -->