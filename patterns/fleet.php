<?php
 /**
  * Title: Fleet
  * Slug: kingcabs/fleet
  * Categories: kingcabs
  */
?>
<!-- wp:group {"style":{"spacing":{"padding":{"top":"100px","bottom":"100px","right":"20px","left":"20px"}},"color":{"background":"#fefefe"}},"layout":{"inherit":true,"type":"constrained"}} -->
<div class="wp-block-group has-background" style="background-color:#fefefe;padding-top:100px;padding-right:20px;padding-bottom:100px;padding-left:20px"><!-- wp:group {"align":"full","layout":{"type":"constrained"}} -->
<div class="wp-block-group alignfull"><!-- wp:heading {"textAlign":"center","style":{"typography":{"fontSize":"30px"}}} -->
<h2 class="wp-block-heading has-text-align-center" style="font-size:30px">MEET THE FLEET</h2>
<!-- /wp:heading -->

<!-- wp:group {"layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"center"}} -->
<div class="wp-block-group"><!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:separator {"backgroundColor":"foreground"} -->
<hr class="wp-block-separator has-text-color has-foreground-color has-alpha-channel-opacity has-foreground-background-color has-background"/>
<!-- /wp:separator --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"color":{"text":"#d9c77e"}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group has-text-color" style="color:#d9c77e"></div>
<!-- /wp:group -->

<!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:separator {"backgroundColor":"foreground"} -->
<hr class="wp-block-separator has-text-color has-foreground-color has-alpha-channel-opacity has-foreground-background-color has-background"/>
<!-- /wp:separator --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:paragraph {"align":"center","style":{"typography":{"fontStyle":"normal","fontWeight":"300"}},"textColor":"foreground","fontSize":"medium"} -->
<p class="has-text-align-center has-foreground-color has-text-color has-medium-font-size" style="font-style:normal;font-weight:300">Lorem ipsum dolor sit amet, consectetuer adipiscing elit enean commodo ligula.</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group -->

<!-- wp:group {"align":"full","style":{"spacing":{"padding":{"top":"100px","bottom":"100px","right":"20px","left":"20px"},"blockGap":"50px"},"border":{"width":"0px","style":"none"}},"backgroundColor":"white","layout":{"inherit":true,"type":"constrained"}} -->
<div class="wp-block-group alignfull has-white-background-color has-background" style="border-style:none;border-width:0px;padding-top:100px;padding-right:20px;padding-bottom:100px;padding-left:20px"><!-- wp:columns {"align":"wide"} -->
<div class="wp-block-columns alignwide"><!-- wp:column -->
<div class="wp-block-column"><!-- wp:group {"style":{"spacing":{"padding":{"top":"20px","bottom":"20px","right":"20px","left":"20px"}}},"className":"has-shadow-dark","layout":{"type":"constrained"}} -->
<div class="wp-block-group has-shadow-dark" style="padding-top:20px;padding-right:20px;padding-bottom:20px;padding-left:20px"><!-- wp:image {"id":163,"width":580,"height":375,"sizeSlug":"full","linkDestination":"none"} -->
<figure class="wp-block-image size-full is-resized"><img src="https://demo.sparkletheme.com/sparkle-fse/limousine/wp-content/uploads/sites/6/2022/12/about-580x375-1.png" alt="" class="wp-image-163" width="580" height="375"/></figure>
<!-- /wp:image -->

<!-- wp:group {"style":{"spacing":{"padding":{"top":"10px","bottom":"10px"}}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group" style="padding-top:10px;padding-bottom:10px"><!-- wp:heading {"textAlign":"center","style":{"typography":{"fontStyle":"normal","fontWeight":"600"}},"fontSize":"extra-small"} -->
<h2 class="wp-block-heading has-text-align-center has-extra-small-font-size" style="font-style:normal;font-weight:600">Beige BMW Sedan 5 2013</h2>
<!-- /wp:heading -->

<!-- wp:buttons {"layout":{"type":"flex","justifyContent":"center"}} -->
<div class="wp-block-buttons"><!-- wp:button {"style":{"color":{"background":"#d7c77e"}},"fontSize":"tiny"} -->
<div class="wp-block-button has-custom-font-size has-tiny-font-size"><a class="wp-block-button__link has-background wp-element-button" style="background-color:#d7c77e">Price &amp; Reserve</a></div>
<!-- /wp:button --></div>
<!-- /wp:buttons --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column -->
<div class="wp-block-column"><!-- wp:group {"style":{"spacing":{"padding":{"top":"20px","bottom":"20px","right":"20px","left":"20px"}}},"className":"has-shadow-dark","layout":{"type":"constrained"}} -->
<div class="wp-block-group has-shadow-dark" style="padding-top:20px;padding-right:20px;padding-bottom:20px;padding-left:20px"><!-- wp:image {"id":179,"width":580,"height":375,"sizeSlug":"full","linkDestination":"none"} -->
<figure class="wp-block-image size-full is-resized"><img src="https://demo.sparkletheme.com/sparkle-fse/limousine/wp-content/uploads/sites/6/2022/12/7-580x375-1.png" alt="" class="wp-image-179" width="580" height="375"/></figure>
<!-- /wp:image -->

<!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:heading {"textAlign":"center","style":{"typography":{"fontStyle":"normal","fontWeight":"600"}},"fontSize":"extra-small"} -->
<h2 class="wp-block-heading has-text-align-center has-extra-small-font-size" style="font-style:normal;font-weight:600">Beige BMW Sedan 5 2013</h2>
<!-- /wp:heading -->

<!-- wp:buttons {"layout":{"type":"flex","justifyContent":"center"}} -->
<div class="wp-block-buttons"><!-- wp:button {"style":{"color":{"background":"#d7c77e"}},"fontSize":"tiny"} -->
<div class="wp-block-button has-custom-font-size has-tiny-font-size"><a class="wp-block-button__link has-background wp-element-button" style="background-color:#d7c77e">Price &amp; Reserve</a></div>
<!-- /wp:button --></div>
<!-- /wp:buttons --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column -->
<div class="wp-block-column"><!-- wp:group {"style":{"spacing":{"padding":{"top":"20px","bottom":"20px","right":"20px","left":"20px"}}},"className":"has-shadow-dark","layout":{"type":"constrained"}} -->
<div class="wp-block-group has-shadow-dark" style="padding-top:20px;padding-right:20px;padding-bottom:20px;padding-left:20px"><!-- wp:image {"id":180,"width":580,"height":375,"sizeSlug":"full","linkDestination":"none"} -->
<figure class="wp-block-image size-full is-resized"><img src="https://demo.sparkletheme.com/sparkle-fse/limousine/wp-content/uploads/sites/6/2022/12/6-580x375-1.png" alt="" class="wp-image-180" width="580" height="375"/></figure>
<!-- /wp:image -->

<!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:heading {"textAlign":"center","style":{"typography":{"fontStyle":"normal","fontWeight":"600"}},"fontSize":"extra-small"} -->
<h2 class="wp-block-heading has-text-align-center has-extra-small-font-size" style="font-style:normal;font-weight:600">Beige BMW Sedan 5 2013</h2>
<!-- /wp:heading -->

<!-- wp:buttons {"layout":{"type":"flex","justifyContent":"center"}} -->
<div class="wp-block-buttons"><!-- wp:button {"style":{"color":{"background":"#d7c77e"}},"fontSize":"tiny"} -->
<div class="wp-block-button has-custom-font-size has-tiny-font-size"><a class="wp-block-button__link has-background wp-element-button" style="background-color:#d7c77e">Price &amp; Reserve</a></div>
<!-- /wp:button --></div>
<!-- /wp:buttons --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:column --></div>
<!-- /wp:columns -->

<!-- wp:columns {"align":"wide"} -->
<div class="wp-block-columns alignwide"><!-- wp:column -->
<div class="wp-block-column"><!-- wp:group {"style":{"spacing":{"padding":{"top":"20px","bottom":"20px","right":"20px","left":"20px"}}},"className":"has-shadow-dark","layout":{"type":"constrained"}} -->
<div class="wp-block-group has-shadow-dark" style="padding-top:20px;padding-right:20px;padding-bottom:20px;padding-left:20px"><!-- wp:image {"id":179,"sizeSlug":"full","linkDestination":"none"} -->
<figure class="wp-block-image size-full"><img src="https://demo.sparkletheme.com/sparkle-fse/limousine/wp-content/uploads/sites/6/2022/12/7-580x375-1.png" alt="" class="wp-image-179"/></figure>
<!-- /wp:image -->

<!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:heading {"textAlign":"center","style":{"typography":{"fontStyle":"normal","fontWeight":"600"}},"fontSize":"extra-small"} -->
<h2 class="wp-block-heading has-text-align-center has-extra-small-font-size" style="font-style:normal;font-weight:600">Beige BMW Sedan 5 2013</h2>
<!-- /wp:heading -->

<!-- wp:buttons {"layout":{"type":"flex","justifyContent":"center"}} -->
<div class="wp-block-buttons"><!-- wp:button {"style":{"color":{"background":"#d7c77e"}},"fontSize":"tiny"} -->
<div class="wp-block-button has-custom-font-size has-tiny-font-size"><a class="wp-block-button__link has-background wp-element-button" style="background-color:#d7c77e">Price &amp; Reserve</a></div>
<!-- /wp:button --></div>
<!-- /wp:buttons --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column -->
<div class="wp-block-column"><!-- wp:group {"style":{"spacing":{"padding":{"top":"20px","bottom":"20px","right":"20px","left":"20px"}}},"className":"has-shadow-dark","layout":{"type":"constrained"}} -->
<div class="wp-block-group has-shadow-dark" style="padding-top:20px;padding-right:20px;padding-bottom:20px;padding-left:20px"><!-- wp:image {"id":163,"width":580,"height":375,"sizeSlug":"full","linkDestination":"none"} -->
<figure class="wp-block-image size-full is-resized"><img src="https://demo.sparkletheme.com/sparkle-fse/limousine/wp-content/uploads/sites/6/2022/12/about-580x375-1.png" alt="" class="wp-image-163" width="580" height="375"/></figure>
<!-- /wp:image -->

<!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:heading {"textAlign":"center","style":{"typography":{"fontStyle":"normal","fontWeight":"600"}},"fontSize":"extra-small"} -->
<h2 class="wp-block-heading has-text-align-center has-extra-small-font-size" style="font-style:normal;font-weight:600">Beige BMW Sedan 5 2013</h2>
<!-- /wp:heading -->

<!-- wp:buttons {"layout":{"type":"flex","justifyContent":"center"}} -->
<div class="wp-block-buttons"><!-- wp:button {"style":{"color":{"background":"#d7c77e"}},"fontSize":"tiny"} -->
<div class="wp-block-button has-custom-font-size has-tiny-font-size"><a class="wp-block-button__link has-background wp-element-button" style="background-color:#d7c77e">Price &amp; Reserve</a></div>
<!-- /wp:button --></div>
<!-- /wp:buttons --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column -->
<div class="wp-block-column"><!-- wp:group {"style":{"spacing":{"padding":{"top":"20px","bottom":"20px","right":"20px","left":"20px"}}},"className":"has-shadow-dark","layout":{"type":"constrained"}} -->
<div class="wp-block-group has-shadow-dark" style="padding-top:20px;padding-right:20px;padding-bottom:20px;padding-left:20px"><!-- wp:image {"id":163,"width":580,"height":375,"sizeSlug":"full","linkDestination":"none"} -->
<figure class="wp-block-image size-full is-resized"><img src="https://demo.sparkletheme.com/sparkle-fse/limousine/wp-content/uploads/sites/6/2022/12/about-580x375-1.png" alt="" class="wp-image-163" width="580" height="375"/></figure>
<!-- /wp:image -->

<!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:heading {"textAlign":"center","style":{"typography":{"fontStyle":"normal","fontWeight":"600"}},"fontSize":"extra-small"} -->
<h2 class="wp-block-heading has-text-align-center has-extra-small-font-size" style="font-style:normal;font-weight:600">Beige BMW Sedan 5 2013</h2>
<!-- /wp:heading -->

<!-- wp:buttons {"layout":{"type":"flex","justifyContent":"center"}} -->
<div class="wp-block-buttons"><!-- wp:button {"style":{"color":{"background":"#d7c77e"}},"fontSize":"tiny"} -->
<div class="wp-block-button has-custom-font-size has-tiny-font-size"><a class="wp-block-button__link has-background wp-element-button" style="background-color:#d7c77e">Price &amp; Reserve</a></div>
<!-- /wp:button --></div>
<!-- /wp:buttons --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:column --></div>
<!-- /wp:columns --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->