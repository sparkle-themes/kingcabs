<?php
 /**
  * Title: Call To Action
  * Slug: kingcabs/cta
  * Categories: kingcabs
  */
?>
<!-- wp:group {"style":{"spacing":{"padding":{"top":"50px","bottom":"80px"}}},"backgroundColor":"black","layout":{"type":"constrained"}} -->
<div class="wp-block-group has-black-background-color has-background" style="padding-top:50px;padding-bottom:80px"><!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:heading {"textAlign":"center","style":{"typography":{"fontSize":"50px","fontStyle":"normal","fontWeight":"700"},"color":{"text":"#d7c77e"}}} -->
<h2 class="wp-block-heading has-text-align-center has-text-color" style="color:#d7c77e;font-size:50px;font-style:normal;font-weight:700"><em>Are You Ready</em></h2>
<!-- /wp:heading -->

<!-- wp:paragraph {"align":"center","style":{"typography":{"fontStyle":"normal","fontWeight":"300"}},"textColor":"background","fontSize":"medium"} -->
<p class="has-text-align-center has-background-color has-text-color has-medium-font-size" style="font-style:normal;font-weight:300">Lorem Ipsum&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
<!-- /wp:paragraph -->

<!-- wp:group {"layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"center"}} -->
<div class="wp-block-group"><!-- wp:group {"textColor":"black","layout":{"type":"constrained"}} -->
<div class="wp-block-group has-black-color has-text-color"><!-- wp:html -->
<p><i class="fa fa-sharp fa-solid fa-phone"></i></p>
<!-- /wp:html --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"spacing":{"blockGap":"8px"}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:heading {"level":3,"style":{"typography":{"fontSize":"24px","fontStyle":"normal","fontWeight":"300"}},"textColor":"background"} -->
<h3 class="wp-block-heading has-background-color has-text-color" style="font-size:24px;font-style:normal;font-weight:300"><br>Call Us 24 hour available:</h3>
<!-- /wp:heading -->

<!-- wp:heading {"textAlign":"center","style":{"color":{"text":"#d7c77e"},"typography":{"fontSize":"30px","fontStyle":"normal","fontWeight":"400"}}} -->
<h2 class="wp-block-heading has-text-align-center has-text-color" style="color:#d7c77e;font-size:30px;font-style:normal;font-weight:400">(415) 770-9781</h2>
<!-- /wp:heading --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->