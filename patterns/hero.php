<?php
 /**
  * Title: Hero Banner
  * Slug: kingcabs/hero
  * Categories: kingcabs
  */
?>
<!-- wp:group {"align":"full","style":{"spacing":{"padding":{"top":"0px","right":"0px","bottom":"0px","left":"0px"},"blockGap":"0px"}}} -->
<div class="wp-block-group alignfull" style="padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px"><!-- wp:cover {"url":"https://demo.sparkletheme.com/sparkle-fse/limousine/wp-content/uploads/sites/6/2022/12/powerful-headlights-particle-view-modern-luxury-cars-parked-indoors-daytime_146671-17395-1.webp","id":17,"dimRatio":50,"overlayColor":"black","align":"full","style":{"spacing":{"padding":{"top":"20vw","right":"30px","bottom":"18vw","left":"30px"}}}} -->
<div class="wp-block-cover alignfull" style="padding-top:20vw;padding-right:30px;padding-bottom:18vw;padding-left:30px"><span aria-hidden="true" class="wp-block-cover__background has-black-background-color has-background-dim"></span><img class="wp-block-cover__image-background wp-image-17" alt="" src="https://demo.sparkletheme.com/sparkle-fse/limousine/wp-content/uploads/sites/6/2022/12/powerful-headlights-particle-view-modern-luxury-cars-parked-indoors-daytime_146671-17395-1.webp" data-object-fit="cover"/><div class="wp-block-cover__inner-container"><!-- wp:group {"align":"wide","style":{"spacing":{"blockGap":"0px"}},"layout":{"inherit":true,"type":"constrained"}} -->
<div class="wp-block-group alignwide"><!-- wp:group {"align":"full","style":{"spacing":{"blockGap":"0px","padding":{"top":"0px","right":"0px","bottom":"0px","left":"0px"}},"typography":{"lineHeight":"1.5"}},"layout":{"inherit":false}} -->
<div class="wp-block-group alignfull" style="padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px;line-height:1.5"><!-- wp:heading {"textAlign":"center","style":{"typography":{"fontStyle":"normal","fontWeight":"800","lineHeight":"1.4","textTransform":"capitalize","fontSize":"72px"}},"textColor":"background","className":"animated animated-fadeInUp"} -->
<h2 class="wp-block-heading has-text-align-center animated animated-fadeInUp has-background-color has-text-color" style="font-size:72px;font-style:normal;font-weight:800;line-height:1.4;text-transform:capitalize">It Was Popularised In The 1960s</h2>
<!-- /wp:heading -->

<!-- wp:heading {"textAlign":"center","align":"full","style":{"typography":{"fontStyle":"normal","fontWeight":"800","lineHeight":"1.8","textTransform":"capitalize","fontSize":"72px"}},"textColor":"background","className":"animated animated-fadeInUp"} -->
<h2 class="wp-block-heading alignfull has-text-align-center animated animated-fadeInUp has-background-color has-text-color" style="font-size:72px;font-style:normal;font-weight:800;line-height:1.8;text-transform:capitalize">With The Release</h2>
<!-- /wp:heading --></div>
<!-- /wp:group -->

<!-- wp:group {"align":"full","style":{"spacing":{"blockGap":"0px"}}} -->
<div class="wp-block-group alignfull"><!-- wp:paragraph {"align":"center","style":{"typography":{"fontStyle":"normal","fontWeight":"300","fontSize":"20px","lineHeight":"1.1"}},"textColor":"background"} -->
<p class="has-text-align-center has-background-color has-text-color" style="font-size:20px;font-style:normal;font-weight:300;line-height:1.1"><em>Lorem Ipsum&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the</em></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"center","style":{"typography":{"fontStyle":"normal","fontWeight":"300","fontSize":"20px","lineHeight":"2"}},"textColor":"background"} -->
<p class="has-text-align-center has-background-color has-text-color" style="font-size:20px;font-style:normal;font-weight:300;line-height:2"><em> 1500s,...</em></p>
<!-- /wp:paragraph -->

<!-- wp:buttons {"className":"animated animated-fadeInUp","layout":{"type":"flex","justifyContent":"center"},"style":{"spacing":{"margin":{"bottom":"20px","top":"15px"},"blockGap":"0px"}}} -->
<div class="wp-block-buttons animated animated-fadeInUp" style="margin-top:15px;margin-bottom:20px"><!-- wp:button {"style":{"border":{"radius":"5px"},"color":{"background":"#d9c77e"}},"className":"is-style-fill"} -->
<div class="wp-block-button is-style-fill"><a class="wp-block-button__link has-background wp-element-button" href="#" style="border-radius:5px;background-color:#d9c77e">Read More</a></div>
<!-- /wp:button --></div>
<!-- /wp:buttons --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div></div>
<!-- /wp:cover -->

<!-- wp:group {"align":"full","style":{"color":{"background":"#222222"},"spacing":{"padding":{"top":"0px","bottom":"0px"}}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group alignfull has-background" style="background-color:#222222;padding-top:0px;padding-bottom:0px"><!-- wp:columns {"align":"wide","style":{"spacing":{"blockGap":{"top":"30px","left":"30px"}}}} -->
<div class="wp-block-columns alignwide"><!-- wp:column -->
<div class="wp-block-column"><!-- wp:group {"style":{"spacing":{"padding":{"bottom":"20px","top":"20px"}}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group" style="padding-top:20px;padding-bottom:20px"><!-- wp:group {"style":{"spacing":{"padding":{"top":"0px"},"blockGap":"15px"}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group" style="padding-top:0px"><!-- wp:heading {"textAlign":"center","style":{"color":{"text":"#d7c77e"}},"fontSize":"small"} -->
<h2 class="wp-block-heading has-text-align-center has-text-color has-small-font-size" style="color:#d7c77e">VARIETY OF CARS</h2>
<!-- /wp:heading -->

<!-- wp:group {"layout":{"type":"constrained","wideSize":"10%"}} -->
<div class="wp-block-group"><!-- wp:separator {"style":{"color":{"background":"#d7c77e"}},"className":"is-style-default"} -->
<hr class="wp-block-separator has-text-color has-alpha-channel-opacity has-background is-style-default" style="background-color:#d7c77e;color:#d7c77e"/>
<!-- /wp:separator --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:paragraph {"align":"center","style":{"typography":{"fontStyle":"normal","fontWeight":"300","fontSize":"20px"},"color":{"text":"#bebebe"},"spacing":{"padding":{"right":"15px","left":"15px"}}}} -->
<p class="has-text-align-center has-text-color" style="color:#bebebe;padding-right:15px;padding-left:15px;font-size:20px;font-style:normal;font-weight:300">Lorem Ipsum&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s,...</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"center","style":{"typography":{"fontStyle":"normal","fontWeight":"700","textTransform":"uppercase"}},"textColor":"background","fontSize":"extra-small"} -->
<p class="has-text-align-center has-background-color has-text-color has-extra-small-font-size" style="font-style:normal;font-weight:700;text-transform:uppercase">Read More</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column {"style":{"color":{"background":"#1f1e1e"}}} -->
<div class="wp-block-column has-background" style="background-color:#1f1e1e"><!-- wp:group {"style":{"spacing":{"padding":{"bottom":"20px","top":"20px"}}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group" style="padding-top:20px;padding-bottom:20px"><!-- wp:group {"style":{"spacing":{"blockGap":"15px"}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:heading {"textAlign":"center","style":{"color":{"text":"#d7c77e"}},"fontSize":"small"} -->
<h2 class="wp-block-heading has-text-align-center has-text-color has-small-font-size" style="color:#d7c77e">VARIETY OF CARS</h2>
<!-- /wp:heading -->

<!-- wp:group {"layout":{"type":"constrained","wideSize":"10%"}} -->
<div class="wp-block-group"><!-- wp:separator {"style":{"color":{"background":"#d7c77e"}},"className":"is-style-default"} -->
<hr class="wp-block-separator has-text-color has-alpha-channel-opacity has-background is-style-default" style="background-color:#d7c77e;color:#d7c77e"/>
<!-- /wp:separator --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:paragraph {"align":"center","style":{"typography":{"fontStyle":"normal","fontWeight":"300","fontSize":"20px"},"color":{"text":"#bebebe"},"spacing":{"padding":{"right":"15px","left":"15px"}}}} -->
<p class="has-text-align-center has-text-color" style="color:#bebebe;padding-right:15px;padding-left:15px;font-size:20px;font-style:normal;font-weight:300">Lorem Ipsum&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s,...</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"center","style":{"typography":{"fontStyle":"normal","fontWeight":"700","textTransform":"uppercase"}},"textColor":"background","fontSize":"extra-small"} -->
<p class="has-text-align-center has-background-color has-text-color has-extra-small-font-size" style="font-style:normal;font-weight:700;text-transform:uppercase">Read More</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column -->
<div class="wp-block-column"><!-- wp:group {"style":{"spacing":{"padding":{"top":"20px","bottom":"20px"}}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group" style="padding-top:20px;padding-bottom:20px"><!-- wp:group {"style":{"spacing":{"blockGap":"15px"}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:heading {"textAlign":"center","style":{"color":{"text":"#d7c77e"}},"fontSize":"small"} -->
<h2 class="wp-block-heading has-text-align-center has-text-color has-small-font-size" style="color:#d7c77e">VARIETY OF CARS</h2>
<!-- /wp:heading -->

<!-- wp:group {"layout":{"type":"constrained","wideSize":"10%"}} -->
<div class="wp-block-group"><!-- wp:separator {"style":{"color":{"background":"#d7c77e"}},"className":"is-style-default"} -->
<hr class="wp-block-separator has-text-color has-alpha-channel-opacity has-background is-style-default" style="background-color:#d7c77e;color:#d7c77e"/>
<!-- /wp:separator --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:paragraph {"align":"center","style":{"typography":{"fontStyle":"normal","fontWeight":"300","fontSize":"20px"},"color":{"text":"#bebebe"},"spacing":{"padding":{"right":"14px","left":"15px"}}}} -->
<p class="has-text-align-center has-text-color" style="color:#bebebe;padding-right:14px;padding-left:15px;font-size:20px;font-style:normal;font-weight:300">Lorem Ipsum&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s,...</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"center","style":{"typography":{"fontStyle":"normal","fontWeight":"700","textTransform":"uppercase"}},"textColor":"background","fontSize":"extra-small"} -->
<p class="has-text-align-center has-background-color has-text-color has-extra-small-font-size" style="font-style:normal;font-weight:700;text-transform:uppercase">Read More</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:column --></div>
<!-- /wp:columns --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->