<?php
 /**
  * Title: Gallery
  * Slug: kingcabs/gallery
  * Categories: kingcabs
  */
?>
<!-- wp:group {"style":{"border":{"width":"0px","style":"none"},"color":{"background":"#fefefe"},"spacing":{"padding":{"top":"50px","bottom":"80px"}}},"layout":{"inherit":true,"type":"constrained"}} -->
<div class="wp-block-group has-background" style="border-style:none;border-width:0px;background-color:#fefefe;padding-top:50px;padding-bottom:80px"><!-- wp:group {"align":"wide","style":{"spacing":{"blockGap":"10px"}},"layout":{"inherit":false}} -->
<div class="wp-block-group alignwide"><!-- wp:group {"style":{"spacing":{"blockGap":"15px"}},"layout":{"inherit":true,"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:heading {"textAlign":"center","style":{"typography":{"lineHeight":"1.3","fontSize":"30px","fontStyle":"normal","fontWeight":"500"}},"className":" animated animated-fadeInUp"} -->
<h2 class="wp-block-heading has-text-align-center animated animated-fadeInUp" style="font-size:30px;font-style:normal;font-weight:500;line-height:1.3">OUR MAIN SERVICES AREA</h2>
<!-- /wp:heading -->

<!-- wp:group {"layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"center"}} -->
<div class="wp-block-group"><!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:separator {"backgroundColor":"foreground"} -->
<hr class="wp-block-separator has-text-color has-foreground-color has-alpha-channel-opacity has-foreground-background-color has-background"/>
<!-- /wp:separator --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"color":{"text":"#d9c77e"}},"layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group has-text-color" style="color:#d9c77e"></div>
<!-- /wp:group -->

<!-- wp:group {"layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group"><!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:separator {"backgroundColor":"foreground"} -->
<hr class="wp-block-separator has-text-color has-foreground-color has-alpha-channel-opacity has-foreground-background-color has-background"/>
<!-- /wp:separator --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:paragraph {"align":"center","style":{"typography":{"fontStyle":"normal","fontWeight":"300"}},"textColor":"foreground","fontSize":"medium"} -->
<p class="has-text-align-center has-foreground-color has-text-color has-medium-font-size" style="font-style:normal;font-weight:300">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:group {"align":"full","style":{"spacing":{"padding":{"top":"100px","right":"20px","bottom":"100px","left":"20px"},"blockGap":"0px"}},"backgroundColor":"white","layout":{"inherit":true,"type":"constrained"}} -->
<div class="wp-block-group alignfull has-white-background-color has-background" style="padding-top:100px;padding-right:20px;padding-bottom:100px;padding-left:20px"><!-- wp:group {"align":"wide","className":"animated animated-fadeInUp","layout":{"inherit":true,"type":"constrained"}} -->
<div class="wp-block-group alignwide animated animated-fadeInUp"><!-- wp:columns {"align":"wide","style":{"spacing":{"padding":{"top":"0px","right":"0px","bottom":"0px","left":"0px"}}}} -->
<div class="wp-block-columns alignwide" style="padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px"><!-- wp:column {"verticalAlignment":"bottom","width":"60%"} -->
<div class="wp-block-column is-vertically-aligned-bottom" style="flex-basis:60%"><!-- wp:group {"className":" animated animated-fadeInUp"} -->
<div class="wp-block-group animated animated-fadeInUp"><!-- wp:cover {"url":"https://demo.sparkletheme.com/sparkle-fse/limousine/wp-content/uploads/sites/6/2022/12/silhouettes-concert-crowd-front-bright-stage-lights-pool-party_43569-176.webp","id":112,"dimRatio":50,"focalPoint":{"x":0.49,"y":0.34},"minHeight":520,"minHeightUnit":"px","customGradient":"linear-gradient(180deg,rgba(0,0,0,0.18) 0%,rgb(0,0,0) 87%)","contentPosition":"bottom left","style":{"spacing":{"padding":{"left":"40px","top":"40px","right":"40px","bottom":"40px"}}}} -->
<div class="wp-block-cover has-custom-content-position is-position-bottom-left" style="padding-top:40px;padding-right:40px;padding-bottom:40px;padding-left:40px;min-height:520px"><span aria-hidden="true" class="wp-block-cover__background has-background-dim wp-block-cover__gradient-background has-background-gradient" style="background:linear-gradient(180deg,rgba(0,0,0,0.18) 0%,rgb(0,0,0) 87%)"></span><img class="wp-block-cover__image-background wp-image-112" alt="" src="https://demo.sparkletheme.com/sparkle-fse/limousine/wp-content/uploads/sites/6/2022/12/silhouettes-concert-crowd-front-bright-stage-lights-pool-party_43569-176.webp" style="object-position:49% 34%" data-object-fit="cover" data-object-position="49% 34%"/><div class="wp-block-cover__inner-container"><!-- wp:group {"style":{"spacing":{"blockGap":"0px"}}} -->
<div class="wp-block-group"><!-- wp:heading {"textAlign":"left","style":{"typography":{"fontStyle":"normal","fontWeight":"600"},"elements":{"link":{"color":{"text":"var:preset|color|white"}}}},"textColor":"background","fontSize":"large"} -->
<h2 class="wp-block-heading has-text-align-left has-background-color has-text-color has-link-color has-large-font-size" style="font-style:normal;font-weight:600"><a href="#">Team Work</a></h2>
<!-- /wp:heading -->

<!-- wp:paragraph {"textColor":"background","fontSize":"normal"} -->
<p class="has-background-color has-text-color has-normal-font-size">House Construction</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div></div>
<!-- /wp:cover --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column {"style":{"spacing":{"padding":{"top":"0px","right":"0px","bottom":"0px","left":"0px"},"blockGap":"0px"}}} -->
<div class="wp-block-column" style="padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px"><!-- wp:group {"className":" animated animated-fadeInUp"} -->
<div class="wp-block-group animated animated-fadeInUp"><!-- wp:cover {"url":"https://demo.sparkletheme.com/sparkle-fse/limousine/wp-content/uploads/sites/6/2022/12/1000_F_316246827_fY1Jl8gon2hG0cwSA8fmn3wdMNLDr6MM.jpg","id":363,"dimRatio":70,"minHeight":250,"minHeightUnit":"px","customGradient":"linear-gradient(180deg,rgba(0,0,0,0.13) 3%,rgba(9,9,9,0.92) 91%)","contentPosition":"bottom left","style":{"spacing":{"padding":{"top":"40px","right":"40px","bottom":"40px","left":"40px"}}}} -->
<div class="wp-block-cover has-custom-content-position is-position-bottom-left" style="padding-top:40px;padding-right:40px;padding-bottom:40px;padding-left:40px;min-height:250px"><span aria-hidden="true" class="wp-block-cover__background has-background-dim-70 has-background-dim wp-block-cover__gradient-background has-background-gradient" style="background:linear-gradient(180deg,rgba(0,0,0,0.13) 3%,rgba(9,9,9,0.92) 91%)"></span><img class="wp-block-cover__image-background wp-image-363" alt="" src="https://demo.sparkletheme.com/sparkle-fse/limousine/wp-content/uploads/sites/6/2022/12/1000_F_316246827_fY1Jl8gon2hG0cwSA8fmn3wdMNLDr6MM.jpg" data-object-fit="cover"/><div class="wp-block-cover__inner-container"><!-- wp:spacer {"height":"27px"} -->
<div style="height:27px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:group {"style":{"spacing":{"blockGap":"0px"}}} -->
<div class="wp-block-group"><!-- wp:heading {"textAlign":"left","style":{"typography":{"fontStyle":"normal","fontWeight":"600"},"elements":{"link":{"color":{"text":"var:preset|color|background"}}}},"textColor":"background","fontSize":"large"} -->
<h2 class="wp-block-heading has-text-align-left has-background-color has-text-color has-link-color has-large-font-size" style="font-style:normal;font-weight:600"><a href="#">Industries</a></h2>
<!-- /wp:heading -->

<!-- wp:paragraph {"textColor":"background","fontSize":"normal"} -->
<p class="has-background-color has-text-color has-normal-font-size">Trust pays off</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div></div>
<!-- /wp:cover --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"spacing":{"padding":{"top":"20px"}}},"className":" animated animated-fadeInUp"} -->
<div class="wp-block-group animated animated-fadeInUp" style="padding-top:20px"><!-- wp:cover {"url":"https://demo.sparkletheme.com/sparkle-fse/limousine/wp-content/uploads/sites/6/2022/12/color-fireworks-night-sky_1161-61.webp","id":110,"dimRatio":80,"minHeight":250,"minHeightUnit":"px","customGradient":"linear-gradient(180deg,rgba(0,0,0,0) 0%,rgb(8,8,8) 84%)","contentPosition":"bottom left","style":{"spacing":{"padding":{"top":"40px","right":"40px","bottom":"40px","left":"40px"}}}} -->
<div class="wp-block-cover has-custom-content-position is-position-bottom-left" style="padding-top:40px;padding-right:40px;padding-bottom:40px;padding-left:40px;min-height:250px"><span aria-hidden="true" class="wp-block-cover__background has-background-dim-80 has-background-dim wp-block-cover__gradient-background has-background-gradient" style="background:linear-gradient(180deg,rgba(0,0,0,0) 0%,rgb(8,8,8) 84%)"></span><img class="wp-block-cover__image-background wp-image-110" alt="" src="https://demo.sparkletheme.com/sparkle-fse/limousine/wp-content/uploads/sites/6/2022/12/color-fireworks-night-sky_1161-61.webp" data-object-fit="cover"/><div class="wp-block-cover__inner-container"><!-- wp:spacer {"height":"0px"} -->
<div style="height:0px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:group {"style":{"spacing":{"blockGap":"0px"}}} -->
<div class="wp-block-group"><!-- wp:heading {"textAlign":"left","style":{"typography":{"fontStyle":"normal","fontWeight":"600"},"elements":{"link":{"color":{"text":"var:preset|color|white"}}}},"fontSize":"large"} -->
<h2 class="wp-block-heading has-text-align-left has-link-color has-large-font-size" style="font-style:normal;font-weight:600"><a href="#">Office</a></h2>
<!-- /wp:heading -->

<!-- wp:paragraph {"textColor":"background","fontSize":"normal"} -->
<p class="has-background-color has-text-color has-normal-font-size">Somewhere on earth</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div></div>
<!-- /wp:cover --></div>
<!-- /wp:group --></div>
<!-- /wp:column --></div>
<!-- /wp:columns -->

<!-- wp:columns {"align":"wide","style":{"spacing":{"padding":{"top":"0px","right":"0px","bottom":"0px","left":"0px"}}}} -->
<div class="wp-block-columns alignwide" style="padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px"><!-- wp:column {"style":{"spacing":{"padding":{"top":"0px","right":"0px","bottom":"0px","left":"0px"},"blockGap":"0px"}}} -->
<div class="wp-block-column" style="padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px"><!-- wp:group {"className":" animated animated-fadeInUp"} -->
<div class="wp-block-group animated animated-fadeInUp"><!-- wp:cover {"url":"https://demo.sparkletheme.com/sparkle-fse/limousine/wp-content/uploads/sites/6/2022/12/image-1-4.png","id":106,"dimRatio":70,"minHeight":250,"minHeightUnit":"px","customGradient":"linear-gradient(180deg,rgba(0,0,0,0.13) 3%,rgba(9,9,9,0.92) 91%)","contentPosition":"bottom left","style":{"spacing":{"padding":{"top":"40px","right":"40px","bottom":"40px","left":"40px"}}}} -->
<div class="wp-block-cover has-custom-content-position is-position-bottom-left" style="padding-top:40px;padding-right:40px;padding-bottom:40px;padding-left:40px;min-height:250px"><span aria-hidden="true" class="wp-block-cover__background has-background-dim-70 has-background-dim wp-block-cover__gradient-background has-background-gradient" style="background:linear-gradient(180deg,rgba(0,0,0,0.13) 3%,rgba(9,9,9,0.92) 91%)"></span><img class="wp-block-cover__image-background wp-image-106" alt="" src="https://demo.sparkletheme.com/sparkle-fse/limousine/wp-content/uploads/sites/6/2022/12/image-1-4.png" data-object-fit="cover"/><div class="wp-block-cover__inner-container"><!-- wp:spacer {"height":"27px"} -->
<div style="height:27px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:group {"style":{"spacing":{"blockGap":"0px"}}} -->
<div class="wp-block-group"><!-- wp:heading {"textAlign":"left","style":{"typography":{"fontStyle":"normal","fontWeight":"600"},"elements":{"link":{"color":{"text":"var:preset|color|background"}}}},"textColor":"background","fontSize":"large"} -->
<h2 class="wp-block-heading has-text-align-left has-background-color has-text-color has-link-color has-large-font-size" style="font-style:normal;font-weight:600"><a href="#">Industries</a></h2>
<!-- /wp:heading -->

<!-- wp:paragraph {"textColor":"background","fontSize":"normal"} -->
<p class="has-background-color has-text-color has-normal-font-size">Trust pays off</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div></div>
<!-- /wp:cover --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"spacing":{"padding":{"top":"20px"}}},"className":" animated animated-fadeInUp"} -->
<div class="wp-block-group animated animated-fadeInUp" style="padding-top:20px"><!-- wp:cover {"url":"https://demo.sparkletheme.com/sparkle-fse/limousine/wp-content/uploads/sites/6/2022/12/image-1-7.png","id":182,"dimRatio":80,"minHeight":250,"minHeightUnit":"px","customGradient":"linear-gradient(180deg,rgba(0,0,0,0) 0%,rgb(8,8,8) 84%)","contentPosition":"bottom left","style":{"spacing":{"padding":{"top":"40px","right":"40px","bottom":"40px","left":"40px"}}}} -->
<div class="wp-block-cover has-custom-content-position is-position-bottom-left" style="padding-top:40px;padding-right:40px;padding-bottom:40px;padding-left:40px;min-height:250px"><span aria-hidden="true" class="wp-block-cover__background has-background-dim-80 has-background-dim wp-block-cover__gradient-background has-background-gradient" style="background:linear-gradient(180deg,rgba(0,0,0,0) 0%,rgb(8,8,8) 84%)"></span><img class="wp-block-cover__image-background wp-image-182" alt="" src="https://demo.sparkletheme.com/sparkle-fse/limousine/wp-content/uploads/sites/6/2022/12/image-1-7.png" data-object-fit="cover"/><div class="wp-block-cover__inner-container"><!-- wp:spacer {"height":"0px"} -->
<div style="height:0px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:group {"style":{"spacing":{"blockGap":"0px"}}} -->
<div class="wp-block-group"><!-- wp:heading {"textAlign":"left","style":{"typography":{"fontStyle":"normal","fontWeight":"600"},"elements":{"link":{"color":{"text":"var:preset|color|white"}}}},"fontSize":"large"} -->
<h2 class="wp-block-heading has-text-align-left has-link-color has-large-font-size" style="font-style:normal;font-weight:600"><a href="#">Office</a></h2>
<!-- /wp:heading -->

<!-- wp:paragraph {"textColor":"background","fontSize":"normal"} -->
<p class="has-background-color has-text-color has-normal-font-size">Somewhere on earth</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div></div>
<!-- /wp:cover --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column {"verticalAlignment":"bottom","width":"31%"} -->
<div class="wp-block-column is-vertically-aligned-bottom" style="flex-basis:31%"><!-- wp:group {"className":" animated animated-fadeInUp"} -->
<div class="wp-block-group animated animated-fadeInUp"><!-- wp:cover {"url":"https://demo.sparkletheme.com/sparkle-fse/limousine/wp-content/uploads/sites/6/2022/12/front-view-generic-brandless-moder-car_110488-502-2.webp","id":98,"dimRatio":60,"focalPoint":{"x":0.83,"y":0.12},"minHeight":520,"minHeightUnit":"px","customGradient":"linear-gradient(180deg,rgba(0,0,0,0.18) 0%,rgb(0,0,0) 87%)","contentPosition":"bottom left","style":{"spacing":{"padding":{"left":"40px","top":"40px","right":"40px","bottom":"40px"}}}} -->
<div class="wp-block-cover has-custom-content-position is-position-bottom-left" style="padding-top:40px;padding-right:40px;padding-bottom:40px;padding-left:40px;min-height:520px"><span aria-hidden="true" class="wp-block-cover__background has-background-dim-60 has-background-dim wp-block-cover__gradient-background has-background-gradient" style="background:linear-gradient(180deg,rgba(0,0,0,0.18) 0%,rgb(0,0,0) 87%)"></span><img class="wp-block-cover__image-background wp-image-98" alt="" src="https://demo.sparkletheme.com/sparkle-fse/limousine/wp-content/uploads/sites/6/2022/12/front-view-generic-brandless-moder-car_110488-502-2.webp" style="object-position:83% 12%" data-object-fit="cover" data-object-position="83% 12%"/><div class="wp-block-cover__inner-container"><!-- wp:group {"style":{"spacing":{"blockGap":"0px"}}} -->
<div class="wp-block-group"><!-- wp:heading {"textAlign":"left","style":{"typography":{"fontStyle":"normal","fontWeight":"600"},"elements":{"link":{"color":{"text":"var:preset|color|white"}}}},"textColor":"background","fontSize":"large"} -->
<h2 class="wp-block-heading has-text-align-left has-background-color has-text-color has-link-color has-large-font-size" style="font-style:normal;font-weight:600"><a href="#">Team Work</a></h2>
<!-- /wp:heading -->

<!-- wp:paragraph {"textColor":"background","fontSize":"normal"} -->
<p class="has-background-color has-text-color has-normal-font-size">Committed and creative</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div></div>
<!-- /wp:cover --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column {"style":{"spacing":{"padding":{"top":"0px","right":"0px","bottom":"0px","left":"0px"},"blockGap":"0px"}}} -->
<div class="wp-block-column" style="padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px"><!-- wp:group {"className":" animated animated-fadeInUp"} -->
<div class="wp-block-group animated animated-fadeInUp"><!-- wp:cover {"url":"https://demo.sparkletheme.com/sparkle-fse/limousine/wp-content/uploads/sites/6/2022/12/front-view-generic-brandless-moder-car_110488-502-1-2-1.png","id":123,"dimRatio":70,"minHeight":250,"minHeightUnit":"px","customGradient":"linear-gradient(180deg,rgba(0,0,0,0.13) 3%,rgba(9,9,9,0.92) 91%)","contentPosition":"bottom left","style":{"spacing":{"padding":{"top":"40px","right":"40px","bottom":"40px","left":"40px"}}}} -->
<div class="wp-block-cover has-custom-content-position is-position-bottom-left" style="padding-top:40px;padding-right:40px;padding-bottom:40px;padding-left:40px;min-height:250px"><span aria-hidden="true" class="wp-block-cover__background has-background-dim-70 has-background-dim wp-block-cover__gradient-background has-background-gradient" style="background:linear-gradient(180deg,rgba(0,0,0,0.13) 3%,rgba(9,9,9,0.92) 91%)"></span><img class="wp-block-cover__image-background wp-image-123" alt="" src="https://demo.sparkletheme.com/sparkle-fse/limousine/wp-content/uploads/sites/6/2022/12/front-view-generic-brandless-moder-car_110488-502-1-2-1.png" data-object-fit="cover"/><div class="wp-block-cover__inner-container"><!-- wp:spacer {"height":"27px"} -->
<div style="height:27px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:group {"style":{"spacing":{"blockGap":"0px"}}} -->
<div class="wp-block-group"><!-- wp:heading {"textAlign":"left","style":{"typography":{"fontStyle":"normal","fontWeight":"600"},"elements":{"link":{"color":{"text":"var:preset|color|background"}}}},"textColor":"background","fontSize":"large"} -->
<h2 class="wp-block-heading has-text-align-left has-background-color has-text-color has-link-color has-large-font-size" style="font-style:normal;font-weight:600"><a href="#">Industries</a></h2>
<!-- /wp:heading -->

<!-- wp:paragraph {"textColor":"background","fontSize":"normal"} -->
<p class="has-background-color has-text-color has-normal-font-size">Trust pays off</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div></div>
<!-- /wp:cover --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"spacing":{"padding":{"top":"20px"}}},"className":" animated animated-fadeInUp"} -->
<div class="wp-block-group animated animated-fadeInUp" style="padding-top:20px"><!-- wp:cover {"url":"https://demo.sparkletheme.com/sparkle-fse/limousine/wp-content/uploads/sites/6/2022/12/service_3_2.jpg","id":416,"dimRatio":80,"minHeight":250,"minHeightUnit":"px","customGradient":"linear-gradient(180deg,rgba(0,0,0,0) 0%,rgb(8,8,8) 84%)","contentPosition":"bottom left","style":{"spacing":{"padding":{"top":"40px","right":"40px","bottom":"40px","left":"40px"}}}} -->
<div class="wp-block-cover has-custom-content-position is-position-bottom-left" style="padding-top:40px;padding-right:40px;padding-bottom:40px;padding-left:40px;min-height:250px"><span aria-hidden="true" class="wp-block-cover__background has-background-dim-80 has-background-dim wp-block-cover__gradient-background has-background-gradient" style="background:linear-gradient(180deg,rgba(0,0,0,0) 0%,rgb(8,8,8) 84%)"></span><img class="wp-block-cover__image-background wp-image-416" alt="" src="https://demo.sparkletheme.com/sparkle-fse/limousine/wp-content/uploads/sites/6/2022/12/service_3_2.jpg" data-object-fit="cover"/><div class="wp-block-cover__inner-container"><!-- wp:spacer {"height":"0px"} -->
<div style="height:0px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:group {"style":{"spacing":{"blockGap":"0px"}}} -->
<div class="wp-block-group"><!-- wp:heading {"textAlign":"left","style":{"typography":{"fontStyle":"normal","fontWeight":"600"},"elements":{"link":{"color":{"text":"var:preset|color|white"}}}},"fontSize":"large"} -->
<h2 class="wp-block-heading has-text-align-left has-link-color has-large-font-size" style="font-style:normal;font-weight:600"><a href="#">Office</a></h2>
<!-- /wp:heading -->

<!-- wp:paragraph {"textColor":"background","fontSize":"normal"} -->
<p class="has-background-color has-text-color has-normal-font-size">Somewhere on earth</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div></div>
<!-- /wp:cover --></div>
<!-- /wp:group --></div>
<!-- /wp:column --></div>
<!-- /wp:columns --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->