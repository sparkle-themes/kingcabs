<?php
 /**
  * Title: Service
  * Slug: kingcabs/service
  * Categories: kingcabs
  */
?>
<!-- wp:group {"style":{"border":{"width":"0px","style":"none"},"color":{"background":"#fefefe"},"spacing":{"padding":{"top":"50px","bottom":"80px"}}},"layout":{"inherit":true,"type":"constrained"}} -->
<div class="wp-block-group has-background" style="border-style:none;border-width:0px;background-color:#fefefe;padding-top:50px;padding-bottom:80px"><!-- wp:group {"align":"wide","style":{"spacing":{"blockGap":"10px","padding":{"bottom":"30px"}}},"layout":{"inherit":false}} -->
<div class="wp-block-group alignwide" style="padding-bottom:30px"><!-- wp:group {"style":{"spacing":{"blockGap":"15px"}},"layout":{"inherit":true,"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:heading {"textAlign":"center","style":{"typography":{"lineHeight":"1.3","fontSize":"30px","fontStyle":"normal","fontWeight":"500","textTransform":"uppercase"}},"className":" animated animated-fadeInUp"} -->
<h2 class="wp-block-heading has-text-align-center animated animated-fadeInUp" style="font-size:30px;font-style:normal;font-weight:500;line-height:1.3;text-transform:uppercase">BOOK YOUR CARS WE SERVE</h2>
<!-- /wp:heading -->

<!-- wp:group {"layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"center"}} -->
<div class="wp-block-group"><!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:separator {"backgroundColor":"foreground"} -->
<hr class="wp-block-separator has-text-color has-foreground-color has-alpha-channel-opacity has-foreground-background-color has-background"/>
<!-- /wp:separator --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"color":{"text":"#d9c77e"}},"layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group has-text-color" style="color:#d9c77e"></div>
<!-- /wp:group -->

<!-- wp:group {"layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group"><!-- wp:group {"style":{"spacing":{"margin":{"top":"2px","bottom":"2px"}}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group" style="margin-top:2px;margin-bottom:2px"><!-- wp:separator {"backgroundColor":"foreground"} -->
<hr class="wp-block-separator has-text-color has-foreground-color has-alpha-channel-opacity has-foreground-background-color has-background"/>
<!-- /wp:separator --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:paragraph {"align":"center","style":{"typography":{"fontStyle":"normal","fontWeight":"300"}},"textColor":"foreground","fontSize":"medium"} -->
<p class="has-text-align-center has-foreground-color has-text-color has-medium-font-size" style="font-style:normal;font-weight:300">Lorem ipsum dolor sit amet, consectetuer adipiscing elit enean commodo ligula.</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group -->

<!-- wp:group {"align":"full","layout":{"type":"constrained"}} -->
<div class="wp-block-group alignfull"><!-- wp:image {"id":68,"width":1024,"height":146,"sizeSlug":"large","linkDestination":"none"} -->
<figure class="wp-block-image size-large is-resized"><img src="https://demo.sparkletheme.com/sparkle-fse/limousine/wp-content/uploads/sites/6/2022/12/1-1024x146.png" alt="" class="wp-image-68" width="1024" height="146"/></figure>
<!-- /wp:image --></div>
<!-- /wp:group -->

<!-- wp:columns {"align":"wide","style":{"spacing":{"padding":{"top":"40px"}}}} -->
<div class="wp-block-columns alignwide" style="padding-top:40px"><!-- wp:column {"verticalAlignment":"center"} -->
<div class="wp-block-column is-vertically-aligned-center"><!-- wp:group {"align":"wide","style":{"spacing":{"blockGap":"10px","padding":{"top":"0px","right":"0px","bottom":"20px","left":"0px"}},"border":{"radius":"5px"}},"backgroundColor":"white","className":"  animated animated-fadeInUp","layout":{"inherit":true,"type":"constrained"}} -->
<div class="wp-block-group alignwide animated animated-fadeInUp has-white-background-color has-background" style="border-radius:5px;padding-top:0px;padding-right:0px;padding-bottom:20px;padding-left:0px"><!-- wp:group -->
<div class="wp-block-group"><!-- wp:group {"align":"full","style":{"color":{"text":"#d9c77e"}},"layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"center"}} -->
<div class="wp-block-group alignfull has-text-color" style="color:#d9c77e"></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:heading {"textAlign":"center","level":3,"style":{"spacing":{"margin":{"top":"20px","right":"10px","bottom":"10px","left":"10px"}},"typography":{"fontSize":"24px","fontStyle":"normal","fontWeight":"500"}}} -->
<h3 class="wp-block-heading has-text-align-center" style="margin-top:20px;margin-right:10px;margin-bottom:10px;margin-left:10px;font-size:24px;font-style:normal;font-weight:500"><a href="#">Online Payment</a></h3>
<!-- /wp:heading -->

<!-- wp:group {"style":{"spacing":{"blockGap":"0"}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:paragraph {"align":"center","style":{"typography":{"fontStyle":"normal","fontWeight":"300"}},"textColor":"foreground","className":"sp-p-lr","fontSize":"medium"} -->
<p class="has-text-align-center sp-p-lr has-foreground-color has-text-color has-medium-font-size" style="font-style:normal;font-weight:300">Lorem Ipsum&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"center","style":{"typography":{"fontStyle":"normal","fontWeight":"300"}},"textColor":"foreground","className":"sp-p-lr","fontSize":"medium"} -->
<p class="has-text-align-center sp-p-lr has-foreground-color has-text-color has-medium-font-size" style="font-style:normal;font-weight:300">1500s,...</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column {"verticalAlignment":"center"} -->
<div class="wp-block-column is-vertically-aligned-center"><!-- wp:group {"align":"wide","style":{"spacing":{"blockGap":"10px","padding":{"top":"0px","right":"0px","bottom":"20px","left":"0px"}},"border":{"radius":"5px"}},"backgroundColor":"white","className":"  animated animated-fadeInUp","layout":{"inherit":true,"type":"constrained"}} -->
<div class="wp-block-group alignwide animated animated-fadeInUp has-white-background-color has-background" style="border-radius:5px;padding-top:0px;padding-right:0px;padding-bottom:20px;padding-left:0px"><!-- wp:group -->
<div class="wp-block-group"><!-- wp:group {"align":"full","style":{"color":{"text":"#d9c77e"}},"layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"center"}} -->
<div class="wp-block-group alignfull has-text-color" style="color:#d9c77e"></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:heading {"textAlign":"center","level":3,"style":{"spacing":{"margin":{"top":"20px","right":"10px","bottom":"10px","left":"10px"}},"typography":{"fontSize":"24px","fontStyle":"normal","fontWeight":"500"}}} -->
<h3 class="wp-block-heading has-text-align-center" style="margin-top:20px;margin-right:10px;margin-bottom:10px;margin-left:10px;font-size:24px;font-style:normal;font-weight:500"><a href="#">Fleet Of Vehicles</a></h3>
<!-- /wp:heading -->

<!-- wp:group {"style":{"spacing":{"blockGap":"0"}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:paragraph {"align":"center","style":{"typography":{"fontStyle":"normal","fontWeight":"300"}},"textColor":"foreground","className":"sp-p-lr","fontSize":"medium"} -->
<p class="has-text-align-center sp-p-lr has-foreground-color has-text-color has-medium-font-size" style="font-style:normal;font-weight:300">Lorem Ipsum&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"center","style":{"typography":{"fontStyle":"normal","fontWeight":"300"}},"textColor":"foreground","className":"sp-p-lr","fontSize":"medium"} -->
<p class="has-text-align-center sp-p-lr has-foreground-color has-text-color has-medium-font-size" style="font-style:normal;font-weight:300"> 1500s,...</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column {"verticalAlignment":"center"} -->
<div class="wp-block-column is-vertically-aligned-center"><!-- wp:group {"align":"wide","style":{"spacing":{"blockGap":"10px","padding":{"top":"0px","right":"0px","bottom":"20px","left":"0px"}},"border":{"radius":"5px"}},"backgroundColor":"white","className":"  animated animated-fadeInUp","layout":{"inherit":true,"type":"constrained"}} -->
<div class="wp-block-group alignwide animated animated-fadeInUp has-white-background-color has-background" style="border-radius:5px;padding-top:0px;padding-right:0px;padding-bottom:20px;padding-left:0px"><!-- wp:group -->
<div class="wp-block-group"><!-- wp:group {"align":"full","style":{"color":{"text":"#d9c77e"}},"layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"center"}} -->
<div class="wp-block-group alignfull has-text-color" style="color:#d9c77e"></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:heading {"textAlign":"center","level":3,"style":{"spacing":{"margin":{"top":"20px","right":"10px","bottom":"10px","left":"10px"}},"typography":{"fontSize":"24px","fontStyle":"normal","fontWeight":"500"}}} -->
<h3 class="wp-block-heading has-text-align-center" style="margin-top:20px;margin-right:10px;margin-bottom:10px;margin-left:10px;font-size:24px;font-style:normal;font-weight:500"><a href="#">Easy Online Booking</a></h3>
<!-- /wp:heading -->

<!-- wp:group {"style":{"spacing":{"blockGap":"0"}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:paragraph {"align":"center","style":{"typography":{"fontStyle":"normal","fontWeight":"300"}},"textColor":"foreground","className":"sp-p-lr","fontSize":"medium"} -->
<p class="has-text-align-center sp-p-lr has-foreground-color has-text-color has-medium-font-size" style="font-style:normal;font-weight:300">Lorem Ipsum&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"center","style":{"typography":{"fontStyle":"normal","fontWeight":"300"}},"textColor":"foreground","className":"sp-p-lr","fontSize":"medium"} -->
<p class="has-text-align-center sp-p-lr has-foreground-color has-text-color has-medium-font-size" style="font-style:normal;font-weight:300">1500s,...</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:column --></div>
<!-- /wp:columns --></div>
<!-- /wp:group -->